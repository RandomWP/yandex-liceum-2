first, second = input(), input()
if first == second:
    print("ничья")
elif (first == "камень" and second == "ножницы") or \
        (first == "ножницы" and second == "бумага") or \
        (first == "бумага" and second == "камень"):
    print("первый")
elif (second == "камень" and first == "ножницы") or \
        (second == "ножницы" and first == "бумага") or \
        (second == "бумага" and first == "камень"):
    print("второй")