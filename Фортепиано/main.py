from sys import argv, exit
from PyQt5 import QtWidgets, QtMultimedia, QtCore
from PyQt5.QtCore import Qt
from os import listdir
import os

KEYS = {Qt.Key_0: "0",
        Qt.Key_1: "1",
        Qt.Key_2: "2",
        Qt.Key_3: "3",
        Qt.Key_4: "4",
        Qt.Key_5: "5",
        Qt.Key_6: "6",
        Qt.Key_7: "7",
        Qt.Key_8: "8",
        Qt.Key_9: "9"}


class MainWindow(QtWidgets.QMainWindow):
    def __init__(self):
        super().__init__()
        self.initUi()
        self.load_notes()

    def initUi(self):
        self.setGeometry(300, 300, 10, 10)
        self.centralwidget = QtWidgets.QWidget(self)
        self.centralLayout = QtWidgets.QHBoxLayout(self.centralwidget)
        self.centralLayout.setContentsMargins(10, 10, 10, 10)
        self.setCentralWidget(self.centralwidget)
        self.add_buttons()

    def add_buttons(self):
        for i in range(10):
            button = QtWidgets.QPushButton(str(i))
            self.centralLayout.addWidget(button)
            button.clicked.connect(self.play)

    def load_notes(self):
        script_dir = os.path.dirname(os.path.realpath(__file__))
        print(script_dir)
        self.players = dict()
        for i in range(10):
            media = QtCore.QUrl.fromLocalFile(script_dir + f"/notes/{i}.mp3")
            content = QtMultimedia.QMediaContent(media)
            self.players[str(i)] = QtMultimedia.QMediaPlayer()
            self.players[str(i)].setMedia(content)

    def play(self):
        btn_text = self.sender().text()
        print(btn_text)
        self.players[btn_text].play()

    def keyPressEvent(self, event):
        self.players[KEYS[event.key()]].play()


def main():
    app = QtWidgets.QApplication(argv)
    window = MainWindow()
    window.show()
    exit(app.exec_())


if __name__ == "__main__":
    main()
