import asyncio
import discord
import re

TOKEN = ""


class DiscordBot(discord.Client):
    async def on_ready(self):
        print(f'{self.user} has connected to Discord!')
        for guild in self.guilds:
            print(
                f'{self.user} подключились к чату:\n'
                f'{guild.name}(id: {guild.id})')

    async def on_message(self, message):
        if re.match("^(set_timer in [0-9]+ hours and [0-5]?[0-9] minutes?)$", message.content) is not None:
            lst = re.findall("[0-9]+", message.content)
            hours, mins = map(int, lst)
            #print(hours, mins)
            self.loop.create_task(self.timer(hours * 3600 + mins * 60, "The X time has came!", message.channel))

    async def timer(self, pause, message, channel):
        await asyncio.sleep(pause)
        await channel.send(message)


cli = DiscordBot()
cli.run(TOKEN)
