import sys

args = sys.argv[1:]
try:
    params = {"num": "--num" in args,
              "count": "--count" in args,
              "sort": "--sort" in args}
    path = tuple(filter(lambda arg: not arg.startswith("--"), args))[0]
    with open((path), "r") as f:
        lines = list(map(lambda line: line.strip(), f.readlines()))
except FileNotFoundError:
    print("ERROR")
    sys.exit()
except IndexError:
    print("ERROR")
    sys.exit()

if params["sort"]:
    lines.sort()
if params["num"]:
    print(*map(lambda el: f"{el[0]} {el[1]}", enumerate(lines)), sep="\n")
else:
    print(*lines, sep="\n")
if params["count"]:
    print("rows count:", len(lines))
