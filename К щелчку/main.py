import pygame
from math import sqrt

class ChasingCircle(object):
    def __init__(self, center, radius, color, step):
        self.center = center
        self.radius = radius
        self.color = color
        self.target = center
        self.step = step

    def set_target(self, coords):
        self.target = coords

    def set_step(self, step):
        self.step = step

    def update(self):
        if self.center == self.target:
            return
        paths = []
        dist = dist_between(self.center, self.target)
        for x_step in (-self.step, 0, self.step):
            for y_step in (-self.step, 0, self.step):
                dist2 = dist_between((self.center[0] + x_step, self.center[1] + y_step),
                                     (self.target))
                paths.append((dist2 - dist, (x_step, y_step)))
        x_step, y_step = min(paths, key=lambda val: val[0])[1]
        self.center = (self.center[0] + x_step, self.center[1] + y_step)

    def draw(self, surface):
        pygame.draw.circle(surface, self.color, self.center, self.radius)


def dist_between(coords1, coords2):
    return sqrt((coords1[0] - coords2[0]) ** 2 + (coords1[1] - coords2[1]) ** 2)


FPS = 100
SCREEN_WIDTH, SCREEN_HEIGHT = 501, 501
running = True
pygame.init()
screen = pygame.display.set_mode((SCREEN_WIDTH, SCREEN_HEIGHT))
clock = pygame.time.Clock()
circle = ChasingCircle((250, 250), 20, pygame.Color("red"), 3)
while running:
    for event in pygame.event.get():
        if event.type == pygame.QUIT:
            running = False
        elif event.type == pygame.MOUSEBUTTONDOWN:
            circle.set_target(event.pos)
    screen.fill(pygame.Color("black"))
    circle.update()
    circle.draw(screen)
    pygame.display.flip()
    clock.tick(FPS)
pygame.quit()
