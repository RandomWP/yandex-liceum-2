from sys import argv, exit
from PyQt5.QtWidgets import QApplication, QWidget, QMainWindow, QFileDialog
from PyQt5.QtGui import QPixmap
from PyQt5 import uic
from PIL import Image, ImageQt


class MainWindow(QMainWindow):
    def __init__(self):
        super().__init__()
        self.initUI()
        self.image = Image.new("RGBA", (100, 100), (0, 0, 0, 0))
        self.update_label()

    def initUI(self):
        uic.loadUi("MainWindow.ui", self)
        self.buttonLoadImage.clicked.connect(self.load_image)
        self.buttonTurnLeft.clicked.connect(self.rotate_left)
        self.buttonTurnRight.clicked.connect(self.rotate_right)
        self.pushButton.clicked.connect(self.save_image)
        self.checkBoxRed.clicked.connect(self.update_label)
        self.checkBoxGreen.clicked.connect(self.update_label)
        self.checkBoxBlue.clicked.connect(self.update_label)

    def update_image_channels(self):
        bands = []
        if self.checkBoxRed.isChecked():
            bands.append(self.image.getchannel("R"))
        else:
            bands.append(Image.new("L", self.image.size, 0))
        if self.checkBoxGreen.isChecked():
            bands.append(self.image.getchannel("G"))
        else:
            bands.append(Image.new("L", self.image.size, 0))
        if self.checkBoxBlue.isChecked():
            bands.append(self.image.getchannel("B"))
        else:
            bands.append(Image.new("L", self.image.size, 0))
        self.res_image = Image.merge("RGB", bands)

    def update_label(self):
        self.update_image_channels()
        self.pixmap = QPixmap.fromImage(ImageQt.ImageQt(self.res_image))
        self.label.setPixmap(self.pixmap)

    def rotate_left(self):
        self.image = self.image.transpose(Image.ROTATE_90)
        self.update_label()

    def rotate_right(self):
        self.image = self.image.transpose(Image.ROTATE_270)
        self.update_label()

    def load_image(self):
        fname = QFileDialog.getOpenFileName(
            self, "Загрузить картинку", "", "Images (*.png *.xpm *.jpg *.jpeg)")[0]
        if fname:
            self.image = Image.open(fname)
            self.image = self.image.convert("RGB")
            self.update_label()

    def save_image(self):
        self.image.save("res.jpg")


def main():
    app = QApplication(argv)
    window = MainWindow()
    window.show()
    exit(app.exec_())


if __name__ == "__main__":
    main()
