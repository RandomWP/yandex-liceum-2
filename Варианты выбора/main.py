from flask import Flask, url_for, request, render_template

app = Flask(__name__)


@app.route("/")
def root():
    return app.send_static_file("html/root.htm")


@app.route("/index")
def index():
    return app.send_static_file("html/index.htm")


@app.route("/promotion")
def promotion():
    return app.send_static_file("html/promotion.htm")


@app.route("/image_mars")
def image_mars():
    return app.send_static_file("html/image_mars.htm")


@app.route("/promotion_image")
def promotion_image():
    return app.send_static_file("html/promotion_image.htm")


@app.route("/astronaut_selection", methods=("POST", "GET"))
def astronaut_selection():
    if request.method == "GET":
        return app.send_static_file("html/astronaut_selection.htm")
    elif request.method == "POST":
        print(request.form)
        return "OK"


@app.route("/choice/<planet>")
def choice(planet):
    reasons = {"марс":
               {"reason0": "Эта планета близка к Земле",
                "reason1": "На ней много необходимых ресурсов",
                "reason2": "На ней есть вода и атмосфера",
                "reason3": "На ней есть небольшое магнитное поле",
                "reason4": "Наконец, она просто красивая"},
                "земля": {"reason0": "Мы отсюда улететь пытаемся"}}
    return render_template("choice.htm", planet=planet, **reasons[planet.lower()])


if __name__ == '__main__':
    app.run(port=8080, host='127.0.0.1', debug=True)
