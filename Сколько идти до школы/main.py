import requests
import math

def lonlat_distance(a, b):
    degree_to_meters_factor = 111 * 1000
    a_lon, a_lat = a
    b_lon, b_lat = b
    radians_lattitude = math.radians((a_lat + b_lat) / 2.)
    lat_lon_factor = math.cos(radians_lattitude)
    dx = abs(a_lon - b_lon) * degree_to_meters_factor * lat_lon_factor
    dy = abs(a_lat - b_lat) * degree_to_meters_factor
    distance = math.sqrt(dx * dx + dy * dy)
    return distance


server_address = "http://geocode-maps.yandex.ru/1.x/"
api_key = "40d1649f-0493-4b70-98ba-98533de7710b"

addresses = []
addresses.append(input("Введите адрес дома: "))
addresses.append(input("Введите адрес школы: "))

coords = []
for address in addresses:
    params = {
        "apikey": api_key,
        "geocode": address,
        "format": "json"
    }
    response = requests.get(server_address, params)
    json_resp = response.json()
    if response:
        coords.append(tuple(map(float, json_resp["response"]["GeoObjectCollection"]
                                ["featureMember"][0]["GeoObject"]["Point"]["pos"].split())))

path_len = lonlat_distance(*coords)
print("Длина пути:", round(path_len, 3), "м")
