from itertools import product


def dig_sum(num):
    return sum(map(int, str(num)))


hours = tuple(map(int, input().split()))
minutes = tuple(map(int, input().split()))


print(*map(lambda tm: str(tm[0]).zfill(2) + ":" + str(tm[1]).zfill(2),
           filter(lambda el: el[0] in hours and
                  el[1] in minutes and
                  dig_sum(el[0]) != dig_sum(el[1]),
                  product(range(24), range(60)))), sep='\n')
