import requests

api_key = "40d1649f-0493-4b70-98ba-98533de7710b"
server_address = "http://geocode-maps.yandex.ru/1.x/"

cities = input().split(",")
cities_coords = []
for city in cities:
    params = {
        "apikey": api_key,
        "geocode": city,
        "format": "json"
    }
    response = requests.get(server_address, params=params)
    if response:
        json_resp = response.json()
        coords = tuple(map(float, json_resp["response"]["GeoObjectCollection"]
                           ["featureMember"][0]["GeoObject"]["Point"]["pos"].split()))
        cities_coords.append((city, coords))
print(min(cities_coords, key=lambda val: val[1][1])[0])
