seq = tuple(input().split(" -> "))
for _ in range(int(input())):
    req = seq.index(input())
    if req == 0:
        print(f"{seq[req]} -> {seq[req + 1]}")
    elif req == len(seq) - 1:
        print(f"{seq[req - 1]} -> {seq[req]}")
    else:
        print(f"{seq[req - 1]} -> {seq[req]} -> {seq[req + 1]}")