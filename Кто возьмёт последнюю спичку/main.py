from sys import argv, exit
from PyQt5 import uic
from PyQt5.QtWidgets import QApplication, QMainWindow, QMessageBox


class MainWindow(QMainWindow):
    def __init__(self):
        super().__init__()
        self.initUi()

    def initUi(self):
        uic.loadUi("MainWindow.ui", self)
        self.lcdNumber.display(37)
        self.pushButton.clicked.connect(self.make_move)
        self.pushButton_2.clicked.connect(self.restart)

    def restart(self):
        self.lcdNumber.display(37)
        self.spinBox.setMaximum(5)
        self.spinBox.setEnabled(True)
        self.pushButton.setEnabled(True)

    def comp_move(self):
        if self.lcdNumber.intValue() <= 5:
            take = self.lcdNumber.intValue() - 1
        elif self.lcdNumber.intValue() == 1:
            take = 1
        else:
            take = 6 - self.spinBox.value()
        self.lcdNumber.display(self.lcdNumber.intValue() - take)
        if self.lcdNumber.intValue() < 5:
            self.spinBox.setMaximum(self.lcdNumber.intValue())
        self.spinBox.setEnabled(True)
        self.pushButton.setEnabled(True)
        if self.lcdNumber.intValue() <= 0:
            self.show_messagebox("Вы проиграли!")
            self.restart()
            return

    def make_move(self):
        self.spinBox.setEnabled(False)
        self.pushButton.setEnabled(False)
        self.lcdNumber.display(
            self.lcdNumber.intValue() - self.spinBox.value())
        if self.lcdNumber.intValue() <= 0:
            self.show_messagebox("Вы проиграли!")
            self.restart()
            return
        self.comp_move()

    def show_messagebox(self, message):
        msgbox = QMessageBox(self)
        msgbox.setText(message)
        msgbox.show()


def main():
    app = QApplication(argv)
    window = MainWindow()
    window.show()
    exit(app.exec_())


if __name__ == "__main__":
    main()
