import json
from sys import stdin

passed = []
for n, line in enumerate(stdin):
    if "ok" in line:
        passed.append(n + 1)
with open("scoring.json") as json_file:
    data = json_file.read()
scoring = json.loads(data)
points = 0
for group in scoring["scoring"]:
    test_count = len(group["required_tests"])
    passed_count = len(
        tuple(filter(lambda test: test in passed, group["required_tests"])))
    points += int(group["points"] * (passed_count / test_count))
print(points)
