from sys import argv, exit
from PyQt5 import uic
from PyQt5.QtWidgets import QApplication, QMainWindow, QTableWidgetItem, QFileDialog
import sqlite3


class MainWindow(QMainWindow):
    def __init__(self):
        super().__init__()
        self.initUi()

    def initUi(self):
        uic.loadUi("MainWindow.ui", self)
        self.pushButton.clicked.connect(self.load_database)
        self.comboBox.activated.connect(self.update_table)

    def load_genres(self):
        self.create_cursor()
        genres = self.execute_query("""SELECT title FROM genres""")
        genres = map(lambda val: val[0], genres)
        for genre in genres:
            self.comboBox.addItem(genre)
        self.close_cursor()

    def request_open_path(self, request, filter):
        path = QFileDialog.getOpenFileName(
            self, request, "", filter)[0]
        if path:
            return path
        raise FileNotFoundError

    def create_cursor(self):
        self.cur = self.db.cursor()

    def execute_query(self, query):
        return self.cur.execute(query)

    def close_cursor(self):
        self.cur.close()

    def load_database(self):
        try:
            path = self.request_open_path(
                "Загрузить базу данных", "Database files (*.db)")
        except FileNotFoundError:
            return
        self.db = sqlite3.connect(path)
        self.load_genres()

    def fill_table(self, header, data):
        self.tableWidget.setRowCount(0)
        self.tableWidget.setColumnCount(0)
        self.tableWidget.setColumnCount(len(header))
        self.tableWidget.setHorizontalHeaderLabels(header)
        self.tableWidget.setRowCount(0)
        for row_num, row_data in enumerate(data):
            self.tableWidget.setRowCount(self.tableWidget.rowCount() + 1)
            for el_num, el in enumerate(row_data):
                item = QTableWidgetItem(str(el))
                self.tableWidget.setItem(row_num, el_num, item)
        self.tableWidget.resizeColumnsToContents()

    def update_table(self):
        self.create_cursor()
        data = self.execute_query(f"""SELECT title, year, duration FROM Films
WHERE genre = (
SELECT id FROM genres
WHERE title = "{self.comboBox.currentText()}")""",)
        self.fill_table(("Название", "Год выхода", "Длительность (мин)"), data)
        self.close_cursor()


def main():
    app = QApplication(argv)
    window = MainWindow()
    window.show()
    exit(app.exec_())


if __name__ == "__main__":
    main()
