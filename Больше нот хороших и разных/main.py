PITCHES = ("до", "ре", "ми", "фа", "соль", "ля", "си")


class Note:
    PITCHES = ("до", "ре", "ми", "фа", "соль", "ля", "си")
    LONG_PITCHES = ("до-о", "ре-э", "ми-и", "фа-а", "со-оль", "ля-а", "си-и")
    PITCH_DICT = dict(zip(PITCHES, LONG_PITCHES))

    def __init__(self, tone, is_long=False):
        self._tone = tone
        self.is_long = is_long

    def __str__(self):
        if self.is_long:
            return Note.PITCH_DICT[self._tone]
        return self._tone


class LoudNote(Note):
    def __str__(self):
        return super().__str__().upper()


class DefaultNote(Note):
    def __init__(self, tone="до", is_long=False):
        super().__init__(tone, is_long)


class NoteWithOctave(Note):
    def __init__(self, tone, octave, is_long=False):
        self._octave = octave
        super().__init__(tone, is_long)

    def __str__(self):
        return f"{super().__str__()} ({self._octave})"
