from telegram.ext import Updater, CommandHandler, MessageHandler, Filters, ConversationHandler
from telegram import ReplyKeyboardMarkup, ReplyKeyboardRemove
import random

HALL1, HALL2, HALL3, HALL4 = range(4)

TOKEN = ""
REQUEST_KWARGS = {"proxy_url": "socks5h://127.0.0.1:9050"}


def start(update, context):
    update.message.reply_text(
        "Добро пожаловать! Пожалуйста, сдайте верхнюю одежду в гардероб!")
    return goto_hall1(update, context)


def stop(update, context):
    update.message.reply_text("Всего доброго, не забудьте забрать верхнюю одежду в гардеробе!",
                              reply_markup=ReplyKeyboardRemove())
    return ConversationHandler.END


def error(update, context):
    update.message.reply_text(f"Update {update} caused {context.error}")


def goto_hall1(update, context):
    keyboard = [["2"], ["Выйти"]]
    msg = "В данном зале представлены картины Древней Греции\n" \
        "Переходы в залы:\n" \
        "2 - амфоры"
    update.message.reply_text(
        msg, reply_markup=ReplyKeyboardMarkup(keyboard, resize_keyboard=True))
    return HALL1


def goto_hall2(update, context):
    keyboard = [["3"]]
    msg = "В данном зале представлены амфоры Древней Греции\n" \
        "Переходы в залы:\n" \
        "3 - оружие"
    update.message.reply_text(
        msg, reply_markup=ReplyKeyboardMarkup(keyboard, resize_keyboard=True))
    return HALL2


def goto_hall3(update, context):
    keyboard = [["4", "1"]]
    msg = "В данном зале представлено оружие Древней Греции\n" \
        "Переходы в залы:\n" \
        "1 - картины\n" \
        "4 - монеты"
    update.message.reply_text(
        msg, reply_markup=ReplyKeyboardMarkup(keyboard, resize_keyboard=True))
    return HALL3


def goto_hall4(update, context):
    keyboard = [["1"]]
    msg = "В данном зале представлены монеты Древней Греции\n" \
        "Переходы в залы:\n" \
        "1 - картины"
    update.message.reply_text(
        msg, reply_markup=ReplyKeyboardMarkup(keyboard, resize_keyboard=True))
    return HALL4


def hall1(update, context):
    msg_text = update.message.text
    if msg_text == "2":
        return goto_hall2(update, context)
    elif msg_text == "Выйти":
        return stop(update, context)


def hall2(update, context):
    msg_text = update.message.text
    if msg_text == "3":
        return goto_hall3(update, context)


def hall3(update, context):
    msg_text = update.message.text
    if msg_text == "4":
        return goto_hall4(update, context)
    elif msg_text == "1":
        return goto_hall1(update, context)


def hall4(update, context):
    msg_text = update.message.text
    if msg_text == "1":
        return goto_hall1(update, context)


def main():
    updater = Updater(TOKEN, request_kwargs=REQUEST_KWARGS, use_context=True)
    dp = updater.dispatcher

    conv_handler = ConversationHandler(
        entry_points=[CommandHandler("start", start)],
        states={
            HALL1: [MessageHandler(Filters.text, hall1)],
            HALL2: [MessageHandler(Filters.text, hall2)],
            HALL3: [MessageHandler(Filters.text, hall3)],
            HALL4: [MessageHandler(Filters.text, hall4)]
        },
        fallbacks=[CommandHandler("stop", stop)]
    )

    dp.add_handler(conv_handler)
    dp.add_error_handler(error)
    updater.start_polling()
    updater.idle()


if __name__ == "__main__":
    main()
