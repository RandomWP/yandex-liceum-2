from sys import argv, exit
from PyQt5 import uic
from PyQt5.QtWidgets import QApplication, QMainWindow, QFileDialog, QMessageBox


class MainWindow(QMainWindow):
    def __init__(self):
        super().__init__()
        self.initUi()
        self.path = ""

    def initUi(self):
        uic.loadUi("MainWindow.ui", self)
        self.action_new.triggered.connect(self.ui_new)
        self.action_open.triggered.connect(self.ui_open)
        self.action_save.triggered.connect(self.ui_save)
        self.action_saveas.triggered.connect(self.ui_saveas)
        self.action_close.triggered.connect(self.ui_close)

    def request_open_path(self, request):
        path = QFileDialog.getOpenFileName(
            self, request, "", "All files (*.*)")[0]
        if path:
            return path
        raise FileNotFoundError

    def requset_save_path(self, request):
        path = QFileDialog.getSaveFileName(
            self, request, "", "All files (*.*)")[0]
        if path:
            return path
        raise FileNotFoundError

    @staticmethod
    def load_file(path):
        with open(path, "r") as f:
            data = f.read()
        return data

    @staticmethod
    def save_file(path, data):
        with open(path, "w") as f:
            f.write(data)

    def show_messagebox(self, message):
        msgbox = QMessageBox()
        msgbox.setText(message)
        msgbox.show()

    def ui_request_path(self, request, saving=False):
        try:
            if saving:
                self.path = self.requset_save_path(request)
            else:
                self.path = self.request_open_path(request)
        except FileNotFoundError:
            self.show_messagebox("Выберите файл")

    def ui_open(self, request):
        self.ui_request_path("Выберите файл")
        self.textBrowser.setPlainText(self.load_file(self.path))

    def ui_save(self):
        if self.path:
            self.save_file(self.path, self.textBrowser.toPlainText())
        else:
            self.ui_saveas()

    def ui_saveas(self):
        self.ui_request_path("Сохранить", True)
        if self.path:
            self.ui_save()

    def ui_new(self):
        if self.path:
            self.ui_save
        self.path = ""
        self.textBrowser.setPlainText("")

    def ui_close(self):
        exit()


def main():
    app = QApplication(argv)
    window = MainWindow()
    window.show()
    exit(app.exec_())


if __name__ == "__main__":
    main()
