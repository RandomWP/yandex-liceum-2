import sqlite3

db_name = input()
db = sqlite3.connect(db_name)
cur = db.cursor()
res = cur.execute(
    """SELECT title FROM Films
    WHERE year >= 1997 AND genre in (
    SELECT id FROM genres
    WHERE title IN ("музыка", "анимация"))
"""
)
print(*map(lambda val: val[0], res), sep="\n")
cur.close()
