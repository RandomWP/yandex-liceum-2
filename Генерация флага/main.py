from sys import argv, exit
from PyQt5.QtWidgets import QApplication, QMainWindow, QMessageBox
from PyQt5 import uic


class MainWindow(QMainWindow):
    def __init__(self):
        super().__init__()
        self.initUI()

    def initUI(self):
        uic.loadUi("MainWindow.ui", self)
        self.pushButton.clicked.connect(self.generate_flag)

    def generate_flag(self):
        flag = []
        for group in (self.buttonGroup, self.buttonGroup_2, self.buttonGroup_3):
            if group.checkedButton() is None:
                return
            flag.append(group.checkedButton().text())
        message = QMessageBox(self)
        message.setText(", ".join(flag))
        message.show()


def main():
    app = QApplication(argv)
    window = MainWindow()
    window.show()
    exit(app.exec_())


if __name__ == "__main__":
    main()
