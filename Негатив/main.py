with open("input.bmp", "br") as inp:
    header = inp.read(54)
    data = inp.read()
data = bytes(map(lambda byte: 255 - byte, data))
with open("res.bmp", "bw") as out:
    out.write(header + data)
