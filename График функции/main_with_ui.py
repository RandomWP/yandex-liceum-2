from sys import argv, exit
from PyQt5 import QtCore, QtGui, QtWidgets
from PyQt5.QtWidgets import QApplication, QMainWindow
from pyqtgraph import PlotWidget
from math import sin, cos, sqrt


class Ui_MainWindow(object):
    def setupUi(self, MainWindow):
        MainWindow.setObjectName("MainWindow")
        MainWindow.resize(745, 580)
        self.centralwidget = QtWidgets.QWidget(MainWindow)
        self.centralwidget.setObjectName("centralwidget")
        self.verticalLayout_2 = QtWidgets.QVBoxLayout(self.centralwidget)
        self.verticalLayout_2.setObjectName("verticalLayout_2")
        self.graphicsView = PlotWidget(self.centralwidget)
        self.graphicsView.setObjectName("graphicsView")
        self.verticalLayout_2.addWidget(self.graphicsView)
        self.horizontalLayout = QtWidgets.QHBoxLayout()
        self.horizontalLayout.setObjectName("horizontalLayout")
        self.groupBox = QtWidgets.QGroupBox(self.centralwidget)
        self.groupBox.setObjectName("groupBox")
        self.horizontalLayout_3 = QtWidgets.QHBoxLayout(self.groupBox)
        self.horizontalLayout_3.setObjectName("horizontalLayout_3")
        self.label_2 = QtWidgets.QLabel(self.groupBox)
        self.label_2.setObjectName("label_2")
        self.horizontalLayout_3.addWidget(self.label_2)
        self.lineEdit_expr = QtWidgets.QLineEdit(self.groupBox)
        self.lineEdit_expr.setObjectName("lineEdit_expr")
        self.horizontalLayout_3.addWidget(self.lineEdit_expr)
        self.horizontalLayout.addWidget(self.groupBox)
        self.verticalLayout = QtWidgets.QVBoxLayout()
        self.verticalLayout.setObjectName("verticalLayout")
        self.groupBox_2 = QtWidgets.QGroupBox(self.centralwidget)
        self.groupBox_2.setObjectName("groupBox_2")
        self.gridLayout_2 = QtWidgets.QGridLayout(self.groupBox_2)
        self.gridLayout_2.setObjectName("gridLayout_2")
        self.label_4 = QtWidgets.QLabel(self.groupBox_2)
        self.label_4.setObjectName("label_4")
        self.gridLayout_2.addWidget(self.label_4, 2, 1, 1, 2)
        self.spinBox_max = QtWidgets.QSpinBox(self.groupBox_2)
        self.spinBox_max.setMinimum(-1000000)
        self.spinBox_max.setMaximum(1000000)
        self.spinBox_max.setObjectName("spinBox_max")
        self.gridLayout_2.addWidget(self.spinBox_max, 2, 3, 1, 1)
        self.label_3 = QtWidgets.QLabel(self.groupBox_2)
        self.label_3.setObjectName("label_3")
        self.gridLayout_2.addWidget(self.label_3, 1, 1, 1, 1)
        self.spinBox_min = QtWidgets.QSpinBox(self.groupBox_2)
        self.spinBox_min.setMinimum(-1000000)
        self.spinBox_min.setMaximum(1000000)
        self.spinBox_min.setObjectName("spinBox_min")
        self.gridLayout_2.addWidget(self.spinBox_min, 1, 3, 1, 1)
        self.verticalLayout.addWidget(self.groupBox_2)
        self.pushButton_draw = QtWidgets.QPushButton(self.centralwidget)
        self.pushButton_draw.setObjectName("pushButton_draw")
        self.verticalLayout.addWidget(self.pushButton_draw)
        self.horizontalLayout.addLayout(self.verticalLayout)
        self.verticalLayout_2.addLayout(self.horizontalLayout)
        MainWindow.setCentralWidget(self.centralwidget)

        self.retranslateUi(MainWindow)
        QtCore.QMetaObject.connectSlotsByName(MainWindow)

    def retranslateUi(self, MainWindow):
        _translate = QtCore.QCoreApplication.translate
        MainWindow.setWindowTitle(_translate("MainWindow", "График функции"))
        self.groupBox.setTitle(_translate("MainWindow", "Выражение"))
        self.label_2.setText(_translate("MainWindow", "y ="))
        self.groupBox_2.setTitle(_translate("MainWindow", "Диапазон x"))
        self.label_4.setText(_translate("MainWindow", "До"))
        self.label_3.setText(_translate("MainWindow", "От"))
        self.pushButton_draw.setText(_translate("MainWindow", "Построить"))


class MainWindow(QMainWindow, Ui_MainWindow):
    def __init__(self):
        super().__init__()
        self.initUi()

    def initUi(self):
        self.setupUi(self)
        self.retranslateUi(self)
        self.pushButton_draw.clicked.connect(self.draw_plot)

    def draw_plot(self):
        func = self.lineEdit_expr.text()
        start, stop = self.spinBox_min.value(), self.spinBox_max.value()
        self.graphicsView.clear()
        try:
            self.graphicsView.plot([i for i in range(
                start, stop + 1)], [eval(func) for x in range(start, stop + 1)], pen='b')
        except:
            return


def main():
    app = QApplication(argv)
    window = MainWindow()
    window.show()
    exit(app.exec_())


if __name__ == "__main__":
    main()
