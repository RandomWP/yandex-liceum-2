from sys import argv, exit
from PyQt5 import uic
from PyQt5.QtWidgets import QApplication, QMainWindow
from pyqtgraph import PlotWidget
from math import sin, cos, sqrt


class MainWindow(QMainWindow):
    def __init__(self):
        super().__init__()
        self.initUi()

    def initUi(self):
        uic.loadUi("MainWindow.ui", self)
        self.pushButton_draw.clicked.connect(self.draw_plot)

    def draw_plot(self):
        func = self.lineEdit_expr.text()
        start, stop = self.spinBox_min.value(), self.spinBox_max.value()
        self.graphicsView.clear()
        try:
            self.graphicsView.plot([i for i in range(
                start, stop + 1)], [eval(func) for x in range(start, stop + 1)], pen='b')
        except:
            return


def main():
    app = QApplication(argv)
    window = MainWindow()
    window.show()
    exit(app.exec_())


if __name__ == "__main__":
    main()
