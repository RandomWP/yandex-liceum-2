import requests


class YaMapSearch(object):
    def __init__(self):
        self.server_addr = "https://geocode-maps.yandex.ru/1.x/"
        self.apikey = "40d1649f-0493-4b70-98ba-98533de7710b"

    def search_address(self, address: str):
        self.geocode = address
        self.kind = ""
        self._request()

    def search_ll(self, ll: tuple, kind=""):
        self.geocode = ",".join(map(str, ll))
        self.kind = kind
        self._request()

    def get_ll(self, index: int):
        feature_member = self.json_resp["response"]["GeoObjectCollection"]["featureMember"][index]
        ll_string = feature_member["GeoObject"]["Point"]["pos"]
        return tuple(map(float, ll_string.split()))

    def get_address(self, index: int):
        feature_member = self.json_resp["response"]["GeoObjectCollection"]["featureMember"][index]
        return feature_member["GeoObject"]["metaDataProperty"]["GeocoderMetaData"]["Address"]["formatted"]

    def get_text(self, index: int):
        feature_member = self.json_resp["response"]["GeoObjectCollection"]["featureMember"][index]
        return feature_member["GeoObject"]["metaDataProperty"]["GeocoderMetaData"]["text"]

    def get_point(self, index: int, style: str, color: str = "", size: int = "", content: int = ""):
        ll = self.get_ll(index)
        return YaMapPoint(ll, style, color, size, content)

    def get_postal_code(self, index: int):
        feature_member = self.json_resp["response"]["GeoObjectCollection"]["featureMember"][index]
        try:
            return feature_member["GeoObject"]["metaDataProperty"]["GeocoderMetaData"]["Address"]["postal_code"]
        except KeyError:
            return ""

    def _request(self):
        req_params = {
            "apikey": self.apikey,
            "geocode": self.geocode,
            "format": "json"
        }
        if self.kind:
            req_params["kind"] = self.kind
        response = requests.get(self.server_addr, req_params)
        if not response:
            print(f"Error: {response.status_code} ({response.reason})")
        self.json_resp = response.json()


def main():
    search = YaMapSearch()
    address = input("Address: ")
    try:
        search.search_address(address)
        ll = search.get_ll(0)
        search.search_ll(ll, kind="metro")
        address = search.get_address(0)
        text = search.get_text(0)
        print("Station:", text)
    except IndexError:
        print("Failed to find metro station")


if __name__ == "__main__":
    main()
