import pygame
from operator import sub

FPS = 100
SCREEN_WIDTH, SCREEN_HEIGHT = 501, 501
running = True
pygame.init()
screen = pygame.display.set_mode((SCREEN_WIDTH, SCREEN_HEIGHT))
clock = pygame.time.Clock()
rect_color = pygame.Color("white")
rects = []
new_rect_pos = None
new_rect = None
while running:
    for event in pygame.event.get():
        if event.type == pygame.QUIT:
            running = False
        elif event.type == pygame.MOUSEBUTTONDOWN:
            new_rect_pos = event.pos
        elif event.type == pygame.MOUSEMOTION:
            if new_rect_pos is not None:
                new_rect_size = tuple(map(sub, event.pos, new_rect_pos))
                new_rect = pygame.Rect(new_rect_pos, new_rect_size)
        elif event.type == pygame.MOUSEBUTTONUP:
            if new_rect is not None:
                rects.append(new_rect)
            new_rect_pos = None
            new_rect = None
        elif event.type == pygame.KEYDOWN:
            print("keydown")
            keys = pygame.key.get_pressed()
            if (keys[pygame.K_LCTRL] or keys[pygame.K_RCTRL]) and \
                    keys[pygame.K_z] and rects:
                rects.pop()
    screen.fill(pygame.Color("black"))
    for rect in rects + ([new_rect] if new_rect is not None else []):
        pygame.draw.rect(screen, rect_color, rect, 5)
    pygame.display.flip()
    clock.tick(FPS)
pygame.quit()
