import requests
from sys import exit


server_address = "http://geocode-maps.yandex.ru/1.x/"
api_key = "40d1649f-0493-4b70-98ba-98533de7710b"
request_params = {"geocode": "Москва, Красная площадь, 1"}

request = f"{server_address}?format=json&apikey={api_key}&\
{'&'.join(map(lambda it: f'{it[0]}={it[1]}', request_params.items()))}"
response = requests.get(request)

if response:
    resp_content = response.json()
else:
    print("Error!")
    print(request)
    print("Http status:", response.status_code, "(", response.reason, ")")
    exit(0)

feature_member = resp_content["response"]["GeoObjectCollection"]["featureMember"][0]

coords = feature_member["GeoObject"]["Point"]["pos"]
address = feature_member["GeoObject"]["metaDataProperty"]["GeocoderMetaData"]["Address"]["formatted"]
print(coords)
print(address)
