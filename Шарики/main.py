import pygame


class Ball(object):
    def __init__(self, coords: tuple, heading: int, radius: int) -> None:
        self.coords = coords
        self.heading = heading
        self.radius = radius

    def move(self, step: int) -> None:
        self.coords = tuple(coord + step * head for coord,
                            head in zip(self.coords, self.heading))

    def get_coords(self) -> tuple:
        return self.coords

    def bounce_vertical(self) -> None:
        self.heading = (self.heading[0], -self.heading[1])

    def bounce_horizontal(self) -> None:
        self.heading = (-self.heading[0], self.heading[1])

    def get_radius(self) -> int:
        return self.radius


running = True
balls = []
fps = 100
pix_per_frame = 1
ball_radius = 10
pygame.init()
heigth, width = 500, 500
screen = pygame.display.set_mode((heigth, width))
clock = pygame.time.Clock()
while running:
    screen.fill(pygame.Color("black"))
    for event in pygame.event.get():
        if event.type == pygame.QUIT:
            running = False
        elif event.type == pygame.MOUSEBUTTONDOWN:
            balls.append(Ball(event.pos, (1, -1), ball_radius))
    if not balls:
        continue
    for ball in balls:
        pygame.draw.circle(screen, pygame.Color("white"),
                           ball.get_coords(), ball.get_radius())
        if ball.get_coords()[0] <= ball.get_radius() or ball.get_coords()[0] >= width - ball.get_radius():
            ball.bounce_horizontal()
        if ball.get_coords()[1] <= ball.get_radius() or ball.get_coords()[1] >= heigth - ball.get_radius():
            ball.bounce_vertical()
        ball.move(pix_per_frame)
    pygame.display.flip()
    clock.tick(fps)
