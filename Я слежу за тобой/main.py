import pygame

FPS = 100
SCREEN_WIDTH, SCREEN_HEIGHT = 200, 200
running = True
expose_count = 0
pygame.init()
screen = pygame.display.set_mode((SCREEN_WIDTH, SCREEN_HEIGHT))
clock = pygame.time.Clock()
font = pygame.font.Font(None, 100)

while running:
    for event in pygame.event.get():
        if event.type == pygame.QUIT:
            running = False
        elif event.type == pygame.VIDEOEXPOSE:
            expose_count += 1
    screen.fill(pygame.Color("black"))
    text = font.render(str(expose_count), 1, pygame.Color("red"))
    text_x = SCREEN_WIDTH // 2 - text.get_width() // 2
    text_y = SCREEN_HEIGHT // 2 - text.get_height() // 2
    screen.blit(text, (text_x, text_y))
    pygame.display.flip()
    clock.tick(FPS)
pygame.quit()
