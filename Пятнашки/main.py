from PIL import Image

img = Image.open("image.bmp")
part_size = img.size[0] / 4
for x in range(4):
    for y in range(4):
        if x == y == 3:
            continue
        part = img.crop((part_size * x, part_size * y,
                         part_size * (x + 1), part_size * (y + 1)))
        part.save(f"image{y + 1}{x + 1}.bmp")
