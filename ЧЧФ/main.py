def human_read_format(size):
    names = ["", "К", "М", "Г", "T"]
    step = 0
    while size >= 1024:
        step += 1
        size /= 1024
    return str(round(size)) + names[step] + "Б"
