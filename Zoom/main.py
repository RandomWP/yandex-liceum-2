import pygame


def decart_to_image_coords(coords, image_size):
    return tuple(map(lambda c, s: round(s / 2 + c), coords, image_size))


FPS = 100
SCREEN_WIDTH, SCREEN_HEIGHT = 501, 501
running = True
scale = 10
points = tuple(map(lambda str_part: tuple(map(lambda num: float(num.replace(",", ".")),
                                              str_part[1:-1].split(";"))), input().split(", ")))
pygame.init()
screen = pygame.display.set_mode((SCREEN_WIDTH, SCREEN_HEIGHT))
clock = pygame.time.Clock()
redraw = True
while running:
    for event in pygame.event.get():
        if event.type == pygame.QUIT:
            running = False
        elif event.type == pygame.MOUSEBUTTONDOWN:
            if event.button == 4:
                scale *= 1.05
                redraw = True
            elif event.button == 5:
                scale /= 1.05
                redraw = True
    if redraw:
        redraw = False
        points_pixels = tuple(map(lambda coords:
                                  decart_to_image_coords((coords[0] * scale, -coords[1] * scale),
                                                         (SCREEN_WIDTH, SCREEN_HEIGHT)), points))
        screen.fill(pygame.Color("black"))
        pygame.draw.lines(screen, pygame.Color("white"), True, points_pixels)
        pygame.display.flip()
    clock.tick(FPS)
pygame.quit()
