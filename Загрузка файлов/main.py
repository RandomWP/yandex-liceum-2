import vk_api
import os
import requests
from auth import LOGIN, PASSWORD

GROUP_ID = 193176805
ALBUM_ID = 271003894


def auth_handler():
    key = input("Auth code: ")
    remember = False
    return key, remember


def main():
    login, password = LOGIN, PASSWORD
    vk_session = vk_api.VkApi(login, password, auth_handler=auth_handler)
    try:
        vk_session.auth()
    except vk_api.AuthError as err:
        print(err)
        return
    vk = vk_session.get_api()
    server_address = vk.photos.get_upload_server(
        group_id=GROUP_ID, album_id=ALBUM_ID)["upload_url"]
    print(server_address)
    os.chdir("static/img")
    files = {}
    for n, photo in enumerate(os.listdir()[:5]):
        path = os.path.abspath(photo)
        files[f"file{n + 1}"] = open(path, "rb")
    resp = requests.post(server_address, files=files)
    json_resp = resp.json()
    vk.photos.save(album_id=ALBUM_ID, group_id=GROUP_ID, **json_resp)


if __name__ == "__main__":
    main()
