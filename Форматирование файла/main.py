import argparse


def format_text_block(frame_height: int, frame_width: int, file_name: str):
    try:
        with open(file_name, "r") as f:
            lines = list(map(lambda line: line.strip(),
                             f.readlines()[:frame_height]))
        for n, line in enumerate(lines):
            if len(line) > frame_width:
                lines[n] = line[:frame_width]
                lines.insert(n + 1, line[frame_width:])
        return "\n".join(lines[:frame_height])
    except Exception as e:
        return str(e)


def main():
    parser = argparse.ArgumentParser()
    parser.add_argument("--frame-height", type=int, required=True)
    parser.add_argument("--frame-width", type=int, required=True)
    parser.add_argument("filename", type=str)
    args = parser.parse_args()
    print(format_text_block(args.frame_height, args.frame_width, args.filename))


if __name__ == "__main__":
    main()
