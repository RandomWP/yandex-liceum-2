key_seq_en = "qwertyuiop;asdfghjkl;zxcvbnm"
key_seq_rus = "йцукенгшщзхъ;фывапролджэё;ячсмитьбю"
key_seq_num = "1234567890"


def check_length(passwd: str):
    return len(passwd) > 8


def check_letters(passwd: str):
    upper = False
    lower = False
    for char in passwd:
        if char.isalpha():
            upper = upper or char.isupper()
            lower = lower or char.islower()
        if upper and lower:
            return True
    return False


def check_digit(passwd: str):
    return any(map(lambda char: char.isdigit(), passwd))


def check_seq(passwd: str):
    passwd = passwd.lower()
    while len(passwd) >= 3:
        triplet = passwd[:3]
        passwd = passwd[1:]
        if triplet in key_seq_en or \
                triplet in key_seq_rus or \
                triplet in key_seq_num:
            return False
    return True


def check_password(passwd: str):
    assert check_length(passwd)
    assert check_letters(passwd)
    assert check_digit(passwd)
    assert check_seq(passwd)


password = input()
try:
    check_password(password)
    print("ok")
except AssertionError:
    print("error")
