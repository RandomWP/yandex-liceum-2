from sys import argv, exit
from PyQt5 import uic
from PyQt5.QtWidgets import QApplication, QMainWindow, QTableWidgetItem
from PyQt5.QtGui import QColor
from csv import DictReader
from random import randint


class MainWindow(QMainWindow):
    def __init__(self):
        super().__init__()
        self.load_data()
        self.initUi()
        self.update_table()

    def initUi(self):
        uic.loadUi("MainWindow.ui", self)
        self.load_filters()
        self.checkBox.clicked.connect(self.update_table)
        self.checkBox_2.clicked.connect(self.update_table)
        self.comboBox.activated.connect(self.update_table)
        self.comboBox_2.activated.connect(self.update_table)

    def load_data(self):
        with open("rez.csv") as csv_results:
            reader = DictReader(csv_results, delimiter=',', quotechar='"')
            self.result = tuple(row for row in reader)
        print(self.result)

    def fill_table(self, header, data):
        self.tableWidget.setColumnCount(len(header))
        self.tableWidget.setHorizontalHeaderLabels(header)
        self.tableWidget.setRowCount(0)
        for row_num, row_data in enumerate(data):
            self.tableWidget.setRowCount(self.tableWidget.rowCount() + 1)
            for el_num, el in enumerate(row_data):
                item = QTableWidgetItem(el)
                self.tableWidget.setItem(row_num, el_num, item)
        self.tableWidget.resizeColumnsToContents()

    def load_filters(self):
        schools = map(lambda row: row["login"].split("-")[2], self.result)
        grades = map(lambda row: row["login"].split("-")[3], self.result)
        self.comboBox.addItems(schools)
        self.comboBox_2.addItems(grades)

    def update_table(self):
        no_schools_filter = not self.checkBox.isChecked()
        no_grades_filter = not self.checkBox_2.isChecked()
        matched = []
        for row in self.result:
            login_data = row["login"].split("-")
            if (login_data[2] == self.comboBox.currentText() or no_schools_filter) and (login_data[3] == self.comboBox_2.currentText() or no_grades_filter):
                matched.append(row)
        header = ("login", "user_name", "Score")
        data = sorted(map(lambda row: (
            row["login"], row["user_name"], row["Score"]), matched), key=lambda val: val[-1])
        self.fill_table(header, data)
        self.color_table()

    def color_table(self):
        for row in range(3 if self.tableWidget.rowCount() > 3 else self.tableWidget.rowCount()):
            color = QColor(randint(0, 255), randint(0, 255), randint(0, 255))
            for col in range(self.tableWidget.columnCount()):
                item = self.tableWidget.item(row, col)
                item.setBackground(color)


def main():
    app = QApplication(argv)
    window = MainWindow()
    window.show()
    exit(app.exec_())


if __name__ == "__main__":
    main()
