combinations = {"бумага": ("пират", "камень"),
                "камень": ("ножницы", "ром"),
                "ром": ("бумага", "пират"),
                "пират": ("камень", "ножницы"),
                "ножницы": ("бумага", "ром")}

frst, scnd = input(), input()
if frst == scnd:
    print("ничья")
elif scnd in combinations[frst]:
    print("первый")
else:
    print("второй")
