with open("input.txt", "r") as inp:
    data = inp.read()
data = tuple(map(int, data.split()))
pos, neg, zero = 0, 0, 0
for val in data:
    pos += int(val > 0)
    neg += int(val < 0)
    zero += int(val == 0)
with open("output.txt", "w") as out:
    out.write(str(len(data)) + "\n")
    out.write(" ".join(map(lambda sign, val: f"{sign} {val}" if val else "",
                           ("1", "-1", "0"),
                           (pos, neg, zero))).replace("  ", " ") + "\n")
