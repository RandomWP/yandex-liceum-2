from sys import argv, exit
from PyQt5 import QtCore, QtWidgets, QtGui
from PyQt5.QtWidgets import QMainWindow, QApplication, QFileDialog, QTableWidgetItem, QMessageBox, QLabel
from PyQt5.QtGui import QPixmap
from PyQt5.QtCore import Qt
import sqlite3


class Ui_MainWindow(object):
    def setupUi(self, MainWindow):
        MainWindow.setObjectName("MainWindow")
        MainWindow.resize(800, 600)
        self.centralwidget = QtWidgets.QWidget(MainWindow)
        self.centralwidget.setObjectName("centralwidget")
        self.verticalLayout_2 = QtWidgets.QVBoxLayout(self.centralwidget)
        self.verticalLayout_2.setObjectName("verticalLayout_2")
        self.groupBox = QtWidgets.QGroupBox(self.centralwidget)
        self.groupBox.setObjectName("groupBox")
        self.verticalLayout = QtWidgets.QVBoxLayout(self.groupBox)
        self.verticalLayout.setObjectName("verticalLayout")
        self.gridLayout_3 = QtWidgets.QGridLayout()
        self.gridLayout_3.setObjectName("gridLayout_3")
        self.lineEdit_title = QtWidgets.QLineEdit(self.groupBox)
        sizePolicy = QtWidgets.QSizePolicy(
            QtWidgets.QSizePolicy.Expanding, QtWidgets.QSizePolicy.Fixed)
        sizePolicy.setHorizontalStretch(1)
        sizePolicy.setVerticalStretch(0)
        sizePolicy.setHeightForWidth(
            self.lineEdit_title.sizePolicy().hasHeightForWidth())
        self.lineEdit_title.setSizePolicy(sizePolicy)
        self.lineEdit_title.setObjectName("lineEdit_title")
        self.gridLayout_3.addWidget(self.lineEdit_title, 0, 1, 1, 1)
        self.lineEdit_author = QtWidgets.QLineEdit(self.groupBox)
        sizePolicy = QtWidgets.QSizePolicy(
            QtWidgets.QSizePolicy.Expanding, QtWidgets.QSizePolicy.Fixed)
        sizePolicy.setHorizontalStretch(1)
        sizePolicy.setVerticalStretch(0)
        sizePolicy.setHeightForWidth(
            self.lineEdit_author.sizePolicy().hasHeightForWidth())
        self.lineEdit_author.setSizePolicy(sizePolicy)
        self.lineEdit_author.setObjectName("lineEdit_author")
        self.gridLayout_3.addWidget(self.lineEdit_author, 1, 1, 1, 1)
        self.checkBox_title = QtWidgets.QCheckBox(self.groupBox)
        self.checkBox_title.setObjectName("checkBox_title")
        self.gridLayout_3.addWidget(self.checkBox_title, 0, 0, 1, 1)
        self.checkBox_author = QtWidgets.QCheckBox(self.groupBox)
        self.checkBox_author.setObjectName("checkBox_author")
        self.gridLayout_3.addWidget(self.checkBox_author, 1, 0, 1, 1)
        self.verticalLayout.addLayout(self.gridLayout_3)
        self.pushButton = QtWidgets.QPushButton(self.groupBox)
        self.pushButton.setObjectName("pushButton")
        self.verticalLayout.addWidget(self.pushButton)
        self.verticalLayout_2.addWidget(self.groupBox)
        self.tableWidget = QtWidgets.QTableWidget(self.centralwidget)
        self.tableWidget.setObjectName("tableWidget")
        self.tableWidget.setColumnCount(0)
        self.tableWidget.setRowCount(0)
        self.verticalLayout_2.addWidget(self.tableWidget)
        MainWindow.setCentralWidget(self.centralwidget)
        self.statusbar = QtWidgets.QStatusBar(MainWindow)
        self.statusbar.setObjectName("statusbar")
        MainWindow.setStatusBar(self.statusbar)
        self.menuBar = QtWidgets.QMenuBar(MainWindow)
        self.menuBar.setGeometry(QtCore.QRect(0, 0, 800, 21))
        self.menuBar.setObjectName("menuBar")
        self.menu = QtWidgets.QMenu(self.menuBar)
        self.menu.setObjectName("menu")
        self.menu_2 = QtWidgets.QMenu(self.menuBar)
        self.menu_2.setObjectName("menu_2")
        MainWindow.setMenuBar(self.menuBar)
        self.action_open = QtWidgets.QAction(MainWindow)
        self.action_open.setObjectName("action_open")
        self.action_exit = QtWidgets.QAction(MainWindow)
        self.action_exit.setObjectName("action_exit")
        self.action_fix_images = QtWidgets.QAction(MainWindow)
        self.action_fix_images.setObjectName("action_fix_images")
        self.menu.addAction(self.action_open)
        self.menu.addSeparator()
        self.menu.addAction(self.action_exit)
        self.menu_2.addAction(self.action_fix_images)
        self.menuBar.addAction(self.menu.menuAction())
        self.menuBar.addAction(self.menu_2.menuAction())

        self.retranslateUi(MainWindow)
        QtCore.QMetaObject.connectSlotsByName(MainWindow)

    def retranslateUi(self, MainWindow):
        _translate = QtCore.QCoreApplication.translate
        MainWindow.setWindowTitle(_translate(
            "MainWindow", "Каталог библиотеки"))
        self.groupBox.setTitle(_translate("MainWindow", "Поиск книги"))
        self.checkBox_title.setText(_translate(
            "MainWindow", "Фильтровать по названию"))
        self.checkBox_author.setText(_translate(
            "MainWindow", "Фильтровать по автору"))
        self.pushButton.setText(_translate("MainWindow", "Отфильтровать"))
        self.menu.setTitle(_translate("MainWindow", "Файл"))
        self.menu_2.setTitle(_translate("MainWindow", "Сервис"))
        self.action_open.setText(_translate("MainWindow", "Открыть БД..."))
        self.action_exit.setText(_translate("MainWindow", "Выйти"))
        self.action_fix_images.setText(_translate(
            "MainWindow", "Исправить размер изображений в БД"))


class MainWindow(QMainWindow, Ui_MainWindow):
    def __init__(self):
        super().__init__()
        self.initUi()
        self.db = None
        self.load_default_image()

    def initUi(self):
        self.setupUi(self)
        self.retranslateUi(self)
        self.action_open.triggered.connect(self.load_database)
        self.action_exit.triggered.connect(exit)
        self.pushButton.clicked.connect(self.update_table)
        self.action_exit.triggered.connect(self.fix_db_images)

    def get_col_names(self):
        self.create_cursor()
        self.execute_query_fetchone("SELECT * FROM books")
        names = map(lambda val: val[0], self.cur.description)
        self.close_cursor()
        return tuple(names)

    def request_open_path(self, request, filter):
        path = QFileDialog.getOpenFileName(
            self, request, "", filter)[0]
        if path:
            return path
        raise FileNotFoundError

    def create_cursor(self):
        self.cur = self.db.cursor()

    def execute_query(self, query):
        return self.cur.execute(query)

    def close_cursor(self):
        self.cur.close()

    def show_messagebox(self, message):
        msgbox = QMessageBox(self)
        msgbox.setText(message)
        msgbox.show()

    @staticmethod
    def gen_sql_query(conditions, table_name, cols):
        return f"""SELECT {', '.join(cols)} FROM {table_name}
        WHERE {' AND '.join(conditions)}"""

    def load_database(self):
        try:
            path = self.request_open_path(
                "Загрузить базу данных", "Database files (*.db)")
        except FileNotFoundError:
            return
        self.db = sqlite3.connect(path)
        self.update_table()

    def execute_query_fetchone(self, query):
        return self.cur.execute(query).fetchone()

    def execute_query_fetchall(self, query):
        return self.cur.execute(query).fetchall()

    def fill_table(self, header, data):
        self.tableWidget.setRowCount(0)
        self.tableWidget.setColumnCount(0)
        self.tableWidget.setColumnCount(len(header))
        self.tableWidget.setHorizontalHeaderLabels(header)
        self.tableWidget.setRowCount(0)
        for row_num, row_data in enumerate(data):
            self.tableWidget.setRowCount(self.tableWidget.rowCount() + 1)
            for el_num, el in enumerate(row_data):
                if isinstance(el, bytes):
                    print("bytes!")
                    pixmap = QPixmap()
                    pixmap.loadFromData(el)
                    pixmap = pixmap.scaled(150, 150, Qt.KeepAspectRatio,
                                           Qt.SmoothTransformation)
                    label = QLabel()
                    label.setPixmap(pixmap)
                    self.tableWidget.setCellWidget(row_num, el_num, label)
                else:
                    item = QTableWidgetItem(str(el))
                    self.tableWidget.setItem(row_num, el_num, item)
        self.tableWidget.resizeColumnsToContents()
        self.tableWidget.resizeRowsToContents()

    def update_table(self):
        if self.db is None:
            self.show_messagebox("Сначала откройте БД!")
            return
        title_filter = self.checkBox_title.isChecked()
        author_filter = self.checkBox_author.isChecked()
        conditions = []
        self.create_cursor()
        for filter_state, col, text_field in zip((title_filter, author_filter), ("title", "author"), (self.lineEdit_title, self.lineEdit_author)):
            if not filter_state:
                conditions.append(f"true")
            else:
                if col == "author":
                    conditions.append(f"""{col} IN ({", ".join(str(el[0]) for el in self.execute_query_fetchall(f"SELECT id FROM authors WHERE name LIKE '%{text_field.text()}%'"))})""")
                else:
                    conditions.append(f"{col} LIKE '%{text_field.text()}%'")
        self.close_cursor()
        header = self.get_col_names()
        query = self.gen_sql_query(conditions, "books", "*")
        print(header)
        print(query)
        self.create_cursor()
        try:
            data = self.execute_query(query)
        except sqlite3.OperationalError:
            self.show_messagebox("Неверный фильтр!")
            return
        data = tuple(map(list, data))
        for line in data:
            line[2] = self.execute_query_fetchone(
                f"SELECT name FROM authors WHERE id = {line[2]}")[0] if line[2] is not None else "not set"
            line[4] = self.execute_query_fetchone(
                f"SELECT title FROM genres WHERE id = {line[4]}")[0] if line[4] is not None else "not set"
            line[5] = self.execute_query_fetchone(
                f"SELECT image FROM covers WHERE id = {line[5]}")[0] if line[5] is not None else self.noimage
        self.fill_table(header, data)
        self.close_cursor()

    def load_default_image(self):
        try:
            with open("noimage.png", "rb") as img:
                self.noimage = img.read()
        except FileNotFoundError:
            self.noimage = "NOIMAGE"

    def fix_db_images(self):
        pass


def main():
    app = QApplication(argv)
    window = MainWindow()
    window.show()
    exit(app.exec_())


if __name__ == "__main__":
    main()
