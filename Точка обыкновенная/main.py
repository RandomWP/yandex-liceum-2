class Point:
    def __init__(self, x, y):
        self._coords = (x, y)

    def get_x(self):
        return self._coords[0]

    def get_y(self):
        return self._coords[1]

    def get_coords(self):
        return self._coords
