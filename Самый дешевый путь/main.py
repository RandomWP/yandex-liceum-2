import csv
from itertools import chain

with open("input.csv", "r") as csvfile:
    data = tuple(csv.reader(csvfile, delimiter=";"))
paths = tuple((line[0], line[1], int(line[2])) for line in data[:-1])
start, end, _ = data[-1]
indirect = []
for path in filter(lambda path: path[0] == start, paths):
    for path2 in filter(lambda path: path[1] == end, paths):
        if path[1] == path2[0]:
            indirect.append((path, path2))
direct = tuple(path for path in paths if path[0] == start and path[1] == end)
min_indirect = min(indirect, key=lambda path: path[0][-1] + path[1][-1])
if (min_indirect[0][-1] + min_indirect[1][-1]) < min(direct, key=lambda path: path[-1])[-1]:
    print(min_indirect[0][0], min_indirect[0][1], min_indirect[1][1])
else:
    print(start, end)
