import zipfile
print("\n".join(map(lambda path: "  " * path.strip("/").count("/") + path.strip("/").split("/")[-1],
                    zipfile.ZipFile("input.zip").namelist())))
