from telegram import ReplyKeyboardMarkup, ReplyKeyboardRemove
from telegram.ext import Updater, CommandHandler, MessageHandler, Filters, ConversationHandler
import requests
import urllib

TOKEN = ""
TRANSLATOR_APIKEY = ""
REQUEST_KWARGS = {"proxy_url": "socks5h://127.0.0.1:9050"}

MAIN = 1


def start(update, context):
    keyboard = [["ru > en"], ["/stop"]]
    context.chat_data["to_en"] = True
    update.message.reply_text("Bot started")
    update.message.reply_text("Now translating from Russian to English",
                              reply_markup=ReplyKeyboardMarkup(
                                  keyboard, resize_keyboard=True))
    return MAIN


def stop(update, context):
    keyboard = [["/start"]]
    update.message.reply_text("See you later!",
                              reply_markup=ReplyKeyboardMarkup(keyboard, resize_keyboard=True))
    return ConversationHandler.END


def error(update, context):
    update.message.reply_text(f"Update {update} caused {context.error}")


def translate(update, context):
    server_address = "https://translate.yandex.net/api/v1.5/tr.json/translate"
    lang = "ru-en" if context.chat_data["to_en"] else "en-ru"
    text = update.message.text
    url = f"{server_address}?key={TRANSLATOR_APIKEY}&lang={lang}"
    data = {"text": text}
    resp = requests.post(url, data)
    if not resp:
        update.message.reply_text("Something went wrong!")
        update.message.reply_text(f"Error {resp.status_code} ({resp.reason})")
    resp_json = resp.json()
    trans_text = urllib.parse.unquote(resp_json["text"][0])
    update.message.reply_text(trans_text)


def change_direction(update, context):
    dirchar = update.message.text.split()[1]
    new_dirchar = {">": "<", "<": ">"}[dirchar]
    context.chat_data["to_en"] = not context.chat_data["to_en"]
    keyboard = keyboard = [[f"ru {new_dirchar} en"], ["/stop"]]
    msg = "Russian to English" if context.chat_data["to_en"] else "English to Russian"
    update.message.reply_text("Now translating from " + msg,
                              reply_markup=ReplyKeyboardMarkup(keyboard, resize_keyboard=True))


def main():
    updater = Updater(TOKEN, request_kwargs=REQUEST_KWARGS, use_context=True)
    dp = updater.dispatcher

    conv_handler = ConversationHandler(
        entry_points=[CommandHandler("start", start, pass_chat_data=True)],
        states={
            MAIN: [
                MessageHandler(Filters.regex(
                    "^(ru [<>] en)$"), change_direction, pass_chat_data=True),
                MessageHandler(Filters.text, translate, pass_chat_data=True)
            ]
        },
        fallbacks=[CommandHandler("stop", stop)]
    )

    dp.add_handler(conv_handler)
    dp.add_error_handler(error)
    updater.start_polling()
    updater.idle()


if __name__ == "__main__":
    main()
