import sys
import io
import requests
import argparse
from PIL import Image


def get_spn(toponym):
    toponym_delta_1 = list(
        map(float, toponym["boundedBy"]["Envelope"]['lowerCorner'].split()))
    toponym_delta_2 = list(
        map(float, toponym["boundedBy"]["Envelope"]['upperCorner'].split()))
    delta1 = str(abs(toponym_delta_1[0] - toponym_delta_2[0]))
    delta2 = str(abs(toponym_delta_1[1] - toponym_delta_2[1]))
    return (delta1, delta2)


server_address = "http://geocode-maps.yandex.ru/1.x/"

parser = argparse.ArgumentParser()
parser.add_argument("address", type=str)
args = parser.parse_args()

params = {
    "apikey": "40d1649f-0493-4b70-98ba-98533de7710b",
    "geocode": args.address,
    "format": "json"}

response = requests.get(server_address, params)

if not response:
    print("HTTP status:", response.status_code, "(", response.reason, ")")
    sys.exit(1)

json_resp = response.json()
toponym = json_resp["response"]["GeoObjectCollection"][
    "featureMember"][0]["GeoObject"]
toponym_coodrinates = toponym["Point"]["pos"]
toponym_longitude, toponym_latitude = toponym_coodrinates.split(" ")
delta1, delta2 = get_spn(toponym)

server_address = "http://static-maps.yandex.ru/1.x/"

params = {
    "ll": ",".join([toponym_longitude, toponym_latitude]),
    "spn": ",".join([delta1, delta2]),
    "l": "map",
    "pt": "{},pm2rdm".format(",".join([toponym_longitude, toponym_latitude]))
}

response = requests.get(server_address, params)
Image.open(io.BytesIO(response.content)).show()
