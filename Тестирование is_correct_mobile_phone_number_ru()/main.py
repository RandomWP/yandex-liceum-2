from yandex_testing_lesson import is_correct_mobile_phone_number_ru


def testing_function(func):
    test_data = (("", False),
                 ("abc", False),
                 ("123456789768764", False),
                 ("+79647855660", True),
                 ("88005553535", True),
                 ("98005553535", False),
                 ("-76666661313", False),
                 ("+7 666 420-13-13", True),
                 ("+3808005553535", False),
                 ("+796f7855661", False),
                 ("+7(800)5553535", True),
                 ("+1(888)7877866", False),
                 ("00000000000", False),
                 ("8 800 555-35-35", True),
                 ("8 (800) 555-35-35", True),
                 ("8(8005553535", False))
    for input_data, correct_data in test_data:
        try:
            returned = func(input_data)
        except Exception as exc:
            break
        else:
            if returned != correct_data:
                break
    else:
        print("YES")
        return
    print("NO")


testing_function(is_correct_mobile_phone_number_ru)
