class Bell:
    def __init__(self, *args, **kwargs):
        self._info = args
        self._named_info = sorted(kwargs.items(), key=lambda val: val[0])

    def print_info(self):
        from itertools import starmap
        if not self._info and not self._named_info:
            print("-")
        else:
            print(", ".join(starmap(lambda name, par: f"{name}: {par}",
                                    self._named_info)), ", ".join(self._info),
                  sep=('; ' if self._info and self._named_info else ''))


class LittleBell(Bell):
    def sound(self):
        print("ding")


class BigBell(Bell):
    def __init__(self, *args, **kwargs):
        self._dong = False
        super().__init__(*args, **kwargs)

    def sound(self):
        print("dong" if self._dong else "ding")
        self._dong = not self._dong


class BellTower:
    def __init__(self, *bells):
        self._bells = list(bells)

    def append(self, bell):
        self._bells.append(bell)

    def sound(self):
        for bell in self._bells:
            bell.sound()
        print("...")

    def print_info(self):
        for num, bell in enumerate(self._bells):
            print(num + 1, bell.__class__.__name__)
            bell.print_info()


class SizedBellTower(BellTower):
    def __init__(self, *bells, size=10):
        self._size = size
        super().__init__(*bells[-self._size:])

    def append(self, bell):
        super().append(bell)
        self._bells = self._bells[-self._size:]


class TypedBellTower(BellTower):
    def __init__(self, *bells, bell_type=LittleBell):
        if bell_type not in (LittleBell, BigBell):
            bell_type = LittleBell
        self._type = bell_type
        super().__init__(*filter(lambda el: isinstance(el, self._type), bells))

    def append(self, bell):
        if isinstance(bell, self._type):
            super().append(bell)
