import sys
from PyQt5.QtWidgets import QApplication, QWidget, \
    QPushButton, QLineEdit, QLCDNumber, QLabel, QMessageBox


class Calculator(QWidget):
    def __init__(self):
        super().__init__()
        self._initUI()

    def _initUI(self):
        self.setGeometry(500, 500, 250, 150)
        self.setWindowTitle("Миникалькулятор")
        self.btn = QPushButton("Рассчитать", self)
        self.btn.resize(100, 25)
        self.btn.move(10, 90)
        self.btn.clicked.connect(self._count)
        self.line1 = QLineEdit(self)
        self.line2 = QLineEdit(self)
        self.line1.move(10, 10)
        self.line2.move(10, 50)
        self.output = [QLCDNumber(self) for _ in range(4)]
        for y, lcd in zip(range(10, 200, 30), self.output):
            lcd.move(150, y)
        self.labels = [QLabel(text, self) for text in ("+", "-", "*", "/")]
        for y, label in zip(range(13, 200, 30), self.labels):
            label.move(135, y)

    def _count(self):
        if self.line1.text().isnumeric() and self.line2.text().isnumeric():
            for lcd, op in zip(self.output, (lambda x, y: x + y,
                                             lambda x, y: x - y,
                                             lambda x, y: x * y,
                                             lambda x, y: x / y)):
                try:
                    lcd.display(op(float(self.line1.text()),
                                   float(self.line2.text())))
                except ZeroDivisionError:
                    msg = QMessageBox(self)
                    msg.setText("Деление на ноль!")
                    msg.show()


def main():
    app = QApplication(sys.argv)
    form = Calculator()
    form.show()
    sys.exit(app.exec())


if __name__ == "__main__":
    main()
