from yandex_testing_lesson import is_palindrome


def testing_function(func):
    test_data = (("123456789", False),
                 ("aaaaaaaaaaaa", True),
                 ("k", True),
                 ("", True),
                 ("rf", False),
                 ("lol", True),
                 ("lgbttbgl", True))
    for input_data, correct_data in test_data:
        if func(input_data) != correct_data:
            print("NO")
            return
    print("YES")


testing_function(is_palindrome)
