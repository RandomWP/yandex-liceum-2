import pygame
import os


def load_image(name, colorkey=None):
    fullname = os.path.join('data', name)
    image = pygame.image.load(fullname)
    return image


class MovingObject(pygame.sprite.Sprite):
    def __init__(self, group, image_filename, coords=(0, 0), vel=(0, 0)):
        super().__init__(group)
        self.image = load_image(image_filename)
        self.rect = self.image.get_rect()
        self.rect.x, self.rect.y = list(coords)
        self.velocity = list(vel)

    def update(self):
        self.rect.x += self.velocity[0]
        self.rect.y += self.velocity[1]

    def mirror_image(self, xbool, ybool):
        self.image = pygame.transform.flip(self.image, xbool, ybool)

    def mirror_velocity(self, xbool, ybool):
        if xbool:
            self.velocity[0] = -self.velocity[0]
        if ybool:
            self.velocity[1] = -self.velocity[1]

    def is_on_screen(self, screen_size):
        return 0 <= self.rect.x <= screen_size[0] - self.rect.width and \
            0 <= self.rect.y <= screen_size[1] - self.rect.height


FPS = 100
SCREEN_WIDTH, SCREEN_HEIGTH = 600, 95
running = True
pygame.init()
screen = pygame.display.set_mode((SCREEN_WIDTH, SCREEN_HEIGTH))
clock = pygame.time.Clock()
objects = pygame.sprite.Group()
car = MovingObject(objects, "car2.png", vel=(1, 0))
while running:
    for event in pygame.event.get():
        if event.type == pygame.QUIT:
            running = False
    if not car.is_on_screen((SCREEN_WIDTH, SCREEN_HEIGTH)):
        car.mirror_velocity(True, False)
        car.mirror_image(True, False)
    objects.update()
    screen.fill(pygame.Color("white"))
    objects.draw(screen)
    pygame.display.flip()
    clock.tick(FPS)
pygame.quit()
