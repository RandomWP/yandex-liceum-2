from flask import Flask

app = Flask(__name__)


@app.route("/")
def root():
    return "<h1>Миссия Колонизация Марса</h1>"


@app.route("/index")
def index():
    return "И на Марсе будут яблони цвести!"


@app.route("/promotion")
def promotion():
    contents = """Человечество вырастает из детства.<br>
Человечеству мала одна планета.<br>
Мы сделаем обитаемыми безжизненные пока планеты.<br>
И начнем с Марса!<br>
Присоединяйся!<br>"""
    return contents


if __name__ == '__main__':
    app.run(port=8080, host='127.0.0.1')
