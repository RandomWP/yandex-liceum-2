import asyncio
import discord
import requests

TOKEN = ""


class DiscordBot(discord.Client):
    async def on_ready(self):
        print(f'{self.user} has connected to Discord!')
        for guild in self.guilds:
            print(
                f'{self.user} подключились к чату:\n'
                f'{guild.name}(id: {guild.id})')

    async def on_message(self, message):
        if message.author == self.user:
            return
        if "кот" in message.content.lower():
            resp = requests.get("https://api.thecatapi.com/v1/images/search")
            if not resp:
                await message.channel.send(f"Failed to get a cat (({resp.status_code}) {resp.reason})")
            json_resp = resp.json()
            await message.channel.send(json_resp[0]["url"])
        if "собачк" in message.content.lower():
            resp = requests.get("https://dog.ceo/api/breeds/image/random")
            if not resp:
                await message.channel.send(f"Failed to get a dog (({resp.status_code}) {resp.reason})")
            json_resp = resp.json()
            await message.channel.send(json_resp["message"])


cli = DiscordBot()
cli.run(TOKEN)
