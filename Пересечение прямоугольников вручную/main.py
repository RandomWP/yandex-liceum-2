x1, y1, sx1, sy1 = map(int, input().split())
x2, y2, sx2, sy2 = map(int, input().split())
if x1 > x2 + sx2 or x1 + sx1 < x2 or y1 > y2 + sy2 or y1 + sy1 < y2:
    print("NO")
else:
    print("YES")
