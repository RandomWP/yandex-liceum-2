key_seq_en = "qwertyuiop;asdfghjkl;zxcvbnm"
key_seq_rus = "йцукенгшщзхъ;фывапролджэё;ячсмитьбю"
key_seq_num = "1234567890"

PASSWORDS_FILE = "top 10000 passwd.txt"
WORDS_FILE = "top-9999-words.txt"


class PasswordError(Exception):
    pass


class LengthError(PasswordError):
    pass


class LetterError(PasswordError):
    pass


class DigitError(PasswordError):
    pass


class SequenceError(PasswordError):
    pass


class WordError(PasswordError):
    pass


def check_length(passwd: str):
    if len(passwd) < 9:
        raise LengthError()


def check_letters(passwd: str):
    upper = False
    lower = False
    for char in passwd:
        if char.isalpha():
            upper = upper or char.isupper()
            lower = lower or char.islower()
        if upper and lower:
            return
    raise LetterError()


def check_digit(passwd: str):
    if not any(map(lambda char: char.isdigit(), passwd)):
        raise DigitError()


def check_words(passwd: str, words: tuple):
    for word in words:
        if word in passwd:
            raise WordError()


def check_seq(passwd: str):
    passwd = passwd.lower()
    while len(passwd) >= 3:
        triplet = passwd[:3]
        passwd = passwd[1:]
        if triplet in key_seq_en or \
                triplet in key_seq_rus:
            raise SequenceError()


def main():
    with open(PASSWORDS_FILE, "r") as f:
        passwords = f.read().split()
    with open(WORDS_FILE, "r") as f:
        words = f.read().split()
    checking_funcs = (check_length, check_letters, check_digit,
                      lambda passwd: check_words(passwd, words), check_seq)
    exceptions = {}
    for n, passwd in enumerate(passwords):
        for func in checking_funcs:
            try:
                func(passwd)
            except PasswordError as exc:
                exc_class = exc.__class__
                if exc_class in exceptions:
                    exceptions[exc_class] += 1
                else:
                    exceptions[exc_class] = 1
    print(
        *sorted(map(lambda el: f"{el[0].__name__} - {el[1]}", exceptions.items())), sep="\n")


if __name__ == "__main__":
    main()
