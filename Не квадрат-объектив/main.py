from sys import argv, exit
from PyQt5 import uic
from PyQt5.QtWidgets import QApplication, QWidget, QMainWindow, QColorDialog, QFileDialog
from PyQt5.QtGui import QPixmap
from PIL import Image, ImageQt
from PIL.ImageDraw import Draw
from math import sin, cos, pi


class MainWindow(QMainWindow):
    def __init__(self):
        super().__init__()
        self.initUi()
        self.bg_color = "#000000"
        self.line_color = "#FFFFFF"
        self.update_label()

    def initUi(self):
        uic.loadUi("MainWindow.ui", self)
        self.pushButton_update.clicked.connect(self.update_label)
        self.pushButton_clear.clicked.connect(self.clear_label)
        self.pushButton_ch_bg_color.clicked.connect(self.request_bg_color)
        self.pushButton_ch_line_color.clicked.connect(self.request_line_color)
        self.pushButton_save.clicked.connect(self.save_image)
        self.spinBox_img_size.valueChanged.connect(self.update_label)
        self.spinBox_iters.valueChanged.connect(self.update_label)
        self.spinBox_side_length.valueChanged.connect(self.update_label)
        self.spinBox_sides.valueChanged.connect(self.update_label)
        self.doubleSpinBox_ratio.valueChanged.connect(self.update_label)
        self.spinBox_rotation.valueChanged.connect(self.update_label)
        self.checkBox_reverse_ratio.clicked.connect(self.update_label)

    def request_open_path(self, request, filter):
        path = QFileDialog.getOpenFileName(
            self, request, "", filter)[0]
        if path:
            return path
        raise FileNotFoundError

    def requset_save_path(self, request, filter):
        path = QFileDialog.getSaveFileName(
            self, request, "", filter)[0]
        if path:
            return path
        raise FileNotFoundError

    def request_color(self):
        color = QColorDialog.getColor()
        if color.isValid():
            return color.name()
        else:
            raise ValueError

    @staticmethod
    def image_to_pixmap(image):
        return QPixmap.fromImage(ImageQt.ImageQt(image))

    def request_bg_color(self):
        try:
            self.bg_color = self.request_color()
        except ValueError:
            return

    def request_line_color(self):
        try:
            self.line_color = self.request_color()
        except ValueError:
            return

    def generate_magic_image(self):
        size = self.spinBox_img_size.value()
        side_length = self.spinBox_side_length.value()
        sides = self.spinBox_sides.value()
        ratio = self.doubleSpinBox_ratio.value()
        if self.checkBox_reverse_ratio.isChecked() and ratio > 0:
            ratio = 1 / ratio
        iters = self.spinBox_iters.value()
        rotation = deg_to_rad(self.spinBox_rotation.value())
        self.image = gen_image_wrapped(size, side_length, sides, ratio, iters, rotation,
                                       self.bg_color, self.line_color)

    def save_image(self):
        try:
            path = self.requset_save_path("Save image", "All files")
        except FileNotFoundError:
            return
        image = self.image.convert("RGB")
        image.save(path)

    def update_label(self):
        self.generate_magic_image()
        pixmap = self.image_to_pixmap(self.image)
        self.clear_label()
        self.label.setPixmap(pixmap)

    def clear_label(self):
        self.label.clear()


def gen_coords(radius, sides, shift, rotation):
    return tuple((int(cos(2 * pi * k / sides + rotation) * radius + shift[0]),
                  int(sin(2 * pi * k / sides + rotation) * radius + shift[1])) for k in range(sides))


def gen_image(size, coords, ratio, iters, bg_color, line_color):
    img = Image.new("RGBA", (size, ) * 2, bg_color)
    dr = Draw(img)
    draw_magic(dr, coords, ratio, iters, line_color)
    del dr
    return img


def draw_magic(draw_obj, coords, ratio, iters, line_color):
    draw_obj.polygon(coords, outline=line_color)
    if iters == 1:
        return
    new_coords = map(lambda n: find_part_coords(
        coords[n], coords[n - 1], ratio), range(len(coords)))
    draw_magic(draw_obj, tuple(new_coords), ratio, iters - 1, line_color)


def find_part_coords(coords1, coords2, ratio):
    return ((round(coords1[0] + ratio * coords2[0]) / (1 + ratio)),
            (round(coords1[1] + ratio * coords2[1]) / (1 + ratio)))


def radius_around_polygon(side_length, sides):
    return round(0.5 * side_length / sin(pi / sides))


def gen_image_wrapped(image_size, side_length, sides, ratio, iters, rotation, bg_color, line_color):
    radius = radius_around_polygon(side_length, sides)
    coords = gen_coords(radius, sides, (image_size // 2,) * 2, rotation)
    return gen_image(image_size, coords, ratio, iters, bg_color, line_color)


def deg_to_rad(angle):
    return angle * pi / 180


def main():
    app = QApplication(argv)
    window = MainWindow()
    window.show()
    exit(app.exec_())


if __name__ == "__main__":
    main()
