import sys

params = sys.argv[1:]

if params:
    try:
        print(sum(map(lambda val: -int(val[1]) if val[0] % 2 else int(val[1]),
                      enumerate(params))))
    except Exception as e:
        print(e.__class__.__name__)

else:
    print("NO PARAMS")
