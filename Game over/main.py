import pygame
import os


def load_image(name, colorkey=None):
    fullname = os.path.join('data', name)
    image = pygame.image.load(fullname)
    return image


class MovingObject(pygame.sprite.Sprite):
    def __init__(self, group, image_filename, coords=(0, 0), vel=(0, 0)):
        super().__init__(group)
        self.image = load_image(image_filename)
        self.rect = self.image.get_rect()
        self.rect.x, self.rect.y = list(coords)
        self.velocity = list(vel)

    def update(self):
        self.rect.x += self.velocity[0]
        self.rect.y += self.velocity[1]

    def mirror_image(self, xbool, ybool):
        self.image = pygame.transform.flip(self.image, xbool, ybool)

    def mirror_velocity(self, xbool, ybool):
        if xbool:
            self.velocity[0] = -self.velocity[0]
        if ybool:
            self.velocity[1] = -self.velocity[1]

    def is_on_screen(self, screen_size):
        return 0 <= self.rect.x <= screen_size[0] - self.rect.width and \
            0 <= self.rect.y <= screen_size[1] - self.rect.height

    def stop(self):
        self.velocity = (0, 0)


FPS = 100
SCREEN_WIDTH, SCREEN_HEIGTH = 600, 300
running = True
pygame.init()
screen = pygame.display.set_mode((SCREEN_WIDTH, SCREEN_HEIGTH))
clock = pygame.time.Clock()
objects = pygame.sprite.Group()
game_over_picture = MovingObject(
    objects, "gameover.png", vel=(2, 0), coords=(-600, 0))
while running:
    for event in pygame.event.get():
        if event.type == pygame.QUIT:
            running = False
    if game_over_picture.is_on_screen((SCREEN_WIDTH, SCREEN_HEIGTH)):
        game_over_picture.stop()
    objects.update()
    screen.fill(pygame.Color("blue"))
    objects.draw(screen)
    pygame.display.flip()
    clock.tick(FPS)
pygame.quit()
