from sys import argv, exit
from PyQt5 import QtCore, QtGui, QtWidgets
from PyQt5.QtWidgets import QApplication, QMainWindow, QFileDialog, QTableWidgetItem, QMessageBox
import sqlite3
import datetime


class Ui_MainWindow(object):
    def setupUi(self, MainWindow):
        MainWindow.setObjectName("MainWindow")
        MainWindow.resize(800, 600)
        self.centralwidget = QtWidgets.QWidget(MainWindow)
        self.centralwidget.setObjectName("centralwidget")
        self.verticalLayout = QtWidgets.QVBoxLayout(self.centralwidget)
        self.verticalLayout.setObjectName("verticalLayout")
        self.groupBox = QtWidgets.QGroupBox(self.centralwidget)
        self.groupBox.setObjectName("groupBox")
        self.gridLayout = QtWidgets.QGridLayout(self.groupBox)
        self.gridLayout.setObjectName("gridLayout")
        self.label = QtWidgets.QLabel(self.groupBox)
        self.label.setObjectName("label")
        self.gridLayout.addWidget(self.label, 0, 0, 1, 1)
        self.label_2 = QtWidgets.QLabel(self.groupBox)
        self.label_2.setObjectName("label_2")
        self.gridLayout.addWidget(self.label_2, 0, 1, 1, 1)
        self.comboBox_genre = QtWidgets.QComboBox(self.groupBox)
        sizePolicy = QtWidgets.QSizePolicy(
            QtWidgets.QSizePolicy.Preferred, QtWidgets.QSizePolicy.Fixed)
        sizePolicy.setHorizontalStretch(1)
        sizePolicy.setVerticalStretch(0)
        sizePolicy.setHeightForWidth(
            self.comboBox_genre.sizePolicy().hasHeightForWidth())
        self.comboBox_genre.setSizePolicy(sizePolicy)
        self.comboBox_genre.setObjectName("comboBox_genre")
        self.gridLayout.addWidget(self.comboBox_genre, 1, 2, 1, 1)
        self.lineEdit_title = QtWidgets.QLineEdit(self.groupBox)
        sizePolicy = QtWidgets.QSizePolicy(
            QtWidgets.QSizePolicy.Expanding, QtWidgets.QSizePolicy.Fixed)
        sizePolicy.setHorizontalStretch(1)
        sizePolicy.setVerticalStretch(0)
        sizePolicy.setHeightForWidth(
            self.lineEdit_title.sizePolicy().hasHeightForWidth())
        self.lineEdit_title.setSizePolicy(sizePolicy)
        self.lineEdit_title.setObjectName("lineEdit_title")
        self.gridLayout.addWidget(self.lineEdit_title, 1, 0, 1, 1)
        self.spinBox_year = QtWidgets.QSpinBox(self.groupBox)
        sizePolicy = QtWidgets.QSizePolicy(
            QtWidgets.QSizePolicy.Minimum, QtWidgets.QSizePolicy.Fixed)
        sizePolicy.setHorizontalStretch(1)
        sizePolicy.setVerticalStretch(0)
        sizePolicy.setHeightForWidth(
            self.spinBox_year.sizePolicy().hasHeightForWidth())
        self.spinBox_year.setSizePolicy(sizePolicy)
        self.spinBox_year.setMinimum(1800)
        self.spinBox_year.setMaximum(3000)
        self.spinBox_year.setObjectName("spinBox_year")
        self.gridLayout.addWidget(self.spinBox_year, 1, 1, 1, 1)
        self.label_3 = QtWidgets.QLabel(self.groupBox)
        self.label_3.setObjectName("label_3")
        self.gridLayout.addWidget(self.label_3, 0, 2, 1, 1)
        self.lineEdit_duration = QtWidgets.QLineEdit(self.groupBox)
        sizePolicy = QtWidgets.QSizePolicy(
            QtWidgets.QSizePolicy.Expanding, QtWidgets.QSizePolicy.Fixed)
        sizePolicy.setHorizontalStretch(1)
        sizePolicy.setVerticalStretch(0)
        sizePolicy.setHeightForWidth(
            self.lineEdit_duration.sizePolicy().hasHeightForWidth())
        self.lineEdit_duration.setSizePolicy(sizePolicy)
        self.lineEdit_duration.setObjectName("lineEdit_duration")
        self.gridLayout.addWidget(self.lineEdit_duration, 1, 3, 1, 1)
        self.label_4 = QtWidgets.QLabel(self.groupBox)
        self.label_4.setObjectName("label_4")
        self.gridLayout.addWidget(self.label_4, 0, 3, 1, 1)
        self.verticalLayout.addWidget(self.groupBox)
        self.pushButton_add = QtWidgets.QPushButton(self.centralwidget)
        self.pushButton_add.setObjectName("pushButton_add")
        self.verticalLayout.addWidget(self.pushButton_add)
        self.tableWidget = QtWidgets.QTableWidget(self.centralwidget)
        self.tableWidget.setObjectName("tableWidget")
        self.tableWidget.setColumnCount(0)
        self.tableWidget.setRowCount(0)
        self.verticalLayout.addWidget(self.tableWidget)
        MainWindow.setCentralWidget(self.centralwidget)
        self.menubar = QtWidgets.QMenuBar(MainWindow)
        self.menubar.setGeometry(QtCore.QRect(0, 0, 800, 21))
        self.menubar.setObjectName("menubar")
        self.menu = QtWidgets.QMenu(self.menubar)
        self.menu.setObjectName("menu")
        MainWindow.setMenuBar(self.menubar)
        self.statusbar = QtWidgets.QStatusBar(MainWindow)
        self.statusbar.setObjectName("statusbar")
        MainWindow.setStatusBar(self.statusbar)
        self.action_open = QtWidgets.QAction(MainWindow)
        self.action_open.setObjectName("action_open")
        self.action_exit = QtWidgets.QAction(MainWindow)
        self.action_exit.setObjectName("action_exit")
        self.menu.addAction(self.action_open)
        self.menu.addSeparator()
        self.menu.addAction(self.action_exit)
        self.menubar.addAction(self.menu.menuAction())

        self.retranslateUi(MainWindow)
        QtCore.QMetaObject.connectSlotsByName(MainWindow)

    def retranslateUi(self, MainWindow):
        _translate = QtCore.QCoreApplication.translate
        MainWindow.setWindowTitle(_translate("MainWindow", "Фильмотека"))
        self.groupBox.setTitle(_translate("MainWindow", "Добавить"))
        self.label.setText(_translate("MainWindow", "Название"))
        self.label_2.setText(_translate("MainWindow", "Год выпуска"))
        self.label_3.setText(_translate("MainWindow", "Жанр"))
        self.label_4.setText(_translate("MainWindow", "Продолжительность"))
        self.pushButton_add.setText(_translate("MainWindow", "Добавить"))
        self.menu.setTitle(_translate("MainWindow", "Файл"))
        self.action_open.setText(_translate("MainWindow", "Открыть БД..."))
        self.action_exit.setText(_translate("MainWindow", "Выйти"))


class MainWindow(QMainWindow, Ui_MainWindow):
    def __init__(self):
        super().__init__()
        self.initUi()
        self.db = None

    def initUi(self):
        self.setupUi(self)
        self.retranslateUi(self)
        self.set_max_year()
        self.action_exit.triggered.connect(exit)
        self.action_open.triggered.connect(self.load_database)
        self.pushButton_add.clicked.connect(self.add_film)

    def load_genres(self):
        self.create_cursor()
        genres = self.execute_query("""SELECT title FROM genres""").fetchall()
        genres = map(lambda val: val[0], genres)
        self.comboBox_genre.addItems(genres)
        self.close_cursor()

    def get_col_names(self):
        self.create_cursor()
        self.execute_query_fetchone("SELECT * FROM Films")
        names = map(lambda val: val[0], self.cur.description)
        self.close_cursor()
        return tuple(names)

    def request_open_path(self, request, filter):
        path = QFileDialog.getOpenFileName(
            self, request, "", filter)[0]
        if path:
            return path
        raise FileNotFoundError

    def create_cursor(self):
        self.cur = self.db.cursor()

    def execute_query(self, query):
        return self.cur.execute(query)

    def close_cursor(self):
        self.cur.close()

    def load_database(self):
        try:
            path = self.request_open_path(
                "Загрузить базу данных", "Database files (*.db)")
        except FileNotFoundError:
            return
        self.db = sqlite3.connect(path)
        self.load_genres()
        self.update_table()

    def execute_query_fetchone(self, query):
        return self.cur.execute(query).fetchone()

    def fill_table(self, header, data):
        self.tableWidget.setRowCount(0)
        self.tableWidget.setColumnCount(0)
        self.tableWidget.setColumnCount(len(header))
        self.tableWidget.setHorizontalHeaderLabels(header)
        self.tableWidget.setRowCount(0)
        for row_num, row_data in enumerate(data):
            self.tableWidget.setRowCount(self.tableWidget.rowCount() + 1)
            for el_num, el in enumerate(row_data):
                item = QTableWidgetItem(str(el))
                self.tableWidget.setItem(row_num, el_num, item)
        self.tableWidget.resizeColumnsToContents()

    def set_max_year(self):
        year = datetime.date.today().year
        self.spinBox_year.setMaximum(year)

    def update_table(self):
        self.create_cursor()
        data = self.execute_query("SELECT * FROM Films")
        header = self.get_col_names()
        self.fill_table(header, data)
        self.close_cursor()

    def commit_changes(self):
        self.db.commit()

    def add_film(self):
        if self.db is None:
            self.show_messagebox("Сначала откройте БД!")
            return
        title = self.lineEdit_title.text()
        if not title:
            self.show_messagebox("Введите название!")
            return
        year = self.spinBox_year.value()
        genre = self.comboBox_genre.currentText()
        self.create_cursor()
        genre = self.execute_query_fetchone(f"SELECT id FROM genres WHERE title = '{genre}'")[0]
        duration = self.lineEdit_duration.text()
        if duration and (not duration.isnumeric() or int(duration) <= 0):
            self.show_messagebox("Неверная продолжительность!")
            return
        if self.is_in_db(title, year, genre, duration):
            self.show_messagebox("Такой фильм уже есть в БД!")
            return
        self.create_cursor()
        self.execute_query(f"INSERT INTO Films(title, year, genre, duration) VALUES ('{title}', '{year}', '{genre}', '{duration}')")
        self.close_cursor()
        self.commit_changes()
        self.update_table()

    def is_in_db(self, title, year, genre, duration):
        self.create_cursor()
        result = self.execute_query_fetchone(f"""SELECT * FROM Films
        WHERE title = '{title}' AND year = {year} AND duration = '{duration}' AND genre = (
        SELECT id FROM genres
        WHERE title = '{genre}'
        )""")
        self.close_cursor()
        return bool(result)

    def show_messagebox(self, message):
        msgbox = QMessageBox(self)
        msgbox.setText(message)
        msgbox.show()


def main():
    app = QApplication(argv)
    window = MainWindow()
    window.show()
    exit(app.exec_())


if __name__ == "__main__":
    main()
