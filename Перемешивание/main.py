from sys import argv, exit
from PyQt5 import uic
from PyQt5.QtWidgets import QApplication, QMainWindow


class MainWindow(QMainWindow):
    def __init__(self):
        super().__init__()
        self.initUI()

    def initUI(self):
        uic.loadUi("MainWindow.ui", self)
        self.pushButton.clicked.connect(self.read_file)

    def read_file(self):
        self.plainTextEdit.setPlainText("")
        self.plainTextEdit_2.setPlainText("")
        with open("file.txt", "r") as f:
            odd = True
            for line in f.readlines():
                if odd:
                    self.plainTextEdit.appendPlainText(line.strip())
                else:
                    self.plainTextEdit_2.appendPlainText(line.strip())
                odd = not odd


def main():
    app = QApplication(argv)
    window = MainWindow()
    window.show()
    exit(app.exec_())


if __name__ == "__main__":
    main()
