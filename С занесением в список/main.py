class Point:
    def __init__(self, name, x, y):
        self._name = name
        self._coords = (x, y)

    def get_x(self):
        return self._coords[0]

    def get_y(self):
        return self._coords[1]

    def get_coords(self):
        return self._coords

    def __str__(self):
        return f"{self._name}{self._coords}"

    def __invert__(self):
        return Point(self._name, *reversed(self._coords))

    def __repr__(self):
        return f"Point{(self._name, *self._coords)}"
