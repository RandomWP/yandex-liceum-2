import requests
from sys import exit


def make_request(server, apikey, request_params):
    return f"{server_address}?format=json&apikey={api_key}&\
{'&'.join(map(lambda it: f'{it[0]}={it[1]}', request_params.items()))}"


def get_json(request):
    response = requests.get(request)
    if response:
        return response.json()
    else:
        print("Error!")
        print(request)
        print("Http status:", response.status_code, "(", response.reason, ")")
        exit(0)


server_address = "http://geocode-maps.yandex.ru/1.x/"
api_key = "40d1649f-0493-4b70-98ba-98533de7710b"
address = "Москва, Петровка, 28"
request = make_request(server_address, api_key, {"geocode": address})
response_content = get_json(request)
feature_member = response_content["response"]["GeoObjectCollection"]["featureMember"][0]
geocoder_mdata = feature_member["GeoObject"]["metaDataProperty"]["GeocoderMetaData"]
postal_code = geocoder_mdata["Address"]["postal_code"]
print(postal_code)