import requests

server_address = input()
port = input()
value1, value2 = input(), input()
params = {"a": value1, "b": value2}
response = requests.get(f"{server_address}:{port}", params=params)
if response:
    response = response.json()
    print(" ".join(map(str, sorted(response["result"]))))
    print(response["check"])
