import os
import sys
import requests
import pygame

server_address = "https://static-maps.yandex.ru/1.x/"
params = {
    "l": "map",
    "spn": "0.01,0.01",
    "pt": "37.695884,55.803557,pm2gnl~37.497202,55.855558,pm2lbl~37.555439,55.715572,pm2rdl"
}
map_file = "map.png"
screen_size = (600, 450)
response = requests.get(server_address, params=params)

if not response:
    print("HTTP status:", response.status_code, "(", response.reason, ")")
    sys.exit(1)

with open(map_file, "wb") as f:
    f.write(response.content)

pygame.init()
screen = pygame.display.set_mode(screen_size)
screen.blit(pygame.image.load(map_file), (0, 0))
pygame.display.flip()
os.remove(map_file)
while pygame.event.wait().type != pygame.QUIT:
    pass
pygame.quit()
