import sqlite3

db_name = input()
db = sqlite3.connect(db_name)
cur = db.cursor()
res = cur.execute("""SELECT title from Films
    WHERE title LIKE '%Астерикс%' AND title NOT LIKE '%Обеликс%'
""")
print(*map(lambda val: val[0], res), sep="\n")
cur.close()
