table = {"й": "j", "ц": "c", "у": "u", "к": "k", "е": "e", "н": "n",
         "г": "g", "ш": "sh", "щ": "shh", "з": "z", "х": "h", "ъ": "#",
         "ф": "f", "ы": "y", "в": "v", "а": "a", "п": "p", "р": "r",
         "о": "o", "л": "l", "д": "d", "ж": "zh", "э": "je", "я": "ya",
         "ч": "ch", "с": "s", "м": "m", "и": "i", "т": "t", "ь": "'",
         "б": "b", "ю": "ju", "ё": "jo"}
table.update({item[0].upper(): item[1].capitalize() for item in table.items()})
with open("cyrillic.txt", "r") as inp:
    with open("transliteration.txt", "w") as out:
        for line in inp:
            out.write("".join(map(lambda char:
                                  table[char] if char in table else char, line)))
