from sys import argv, exit
from PyQt5 import QtWidgets

MATRIX = [[1, 0, 0, 1], [1, 1, 1, 1], [0, 0, 0, 0], [1, 0, 1, 0]]


class MainWindow(QtWidgets.QMainWindow):
    def __init__(self):
        super().__init__()
        self.initUi()

    def initUi(self):
        self.setGeometry(300, 300, 10, 10)
        self.centralwidget = QtWidgets.QWidget(self)
        self.gridLayout = QtWidgets.QGridLayout(self.centralwidget)
        self.gridLayout.setContentsMargins(10, 10, 10, 10)
        self.setCentralWidget(self.centralwidget)
        self.draw_pixelart(MATRIX)

    def draw_pixelart(self, matrix):
        for x, line in enumerate(matrix):
            for y, val in enumerate(line):
                if val:
                    button = QtWidgets.QPushButton()
                    self.gridLayout.addWidget(button, x, y)


def main():
    app = QtWidgets.QApplication(argv)
    window = MainWindow()
    window.show()
    exit(app.exec_())


if __name__ == "__main__":
    main()
