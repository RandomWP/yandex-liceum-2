class LittleBell:
    def sound(self):
        print("ding")


class BigBell:
    def __init__(self):
        self._dong = False

    def sound(self):
        print("dong" if self._dong else "ding")
        self._dong = not self._dong


class BellTower:
    def __init__(self, *bells):
        self._bells = list(bells)

    def append(self, bell):
        self._bells.append(bell)

    def sound(self):
        for bell in self._bells:
            bell.sound()
        print("...")
