import pygame

COLOR_MAP = {0: "black", 1: "green"}


class SuperList(list):
    def __getitem__(self, index):
        index = index % len(self)
        return super().__getitem__(index)



class Board(object):
    def __init__(self, width: int, heigth: int, fillval: int = 0) -> None:
        self.width = width
        self.heigth = heigth
        self.board = SuperList([SuperList([fillval] * heigth) for _ in range(width)])
        self.set_view(10, 10, 20)

    def set_view(self, left: int, top: int, cell_size: int) -> None:
        self.left = left
        self.top = top
        self.cell_size = cell_size

    def get_pixel_size(self) -> tuple:
        return (self.cell_size * self.width, self.cell_size * self.heigth)

    def get_pixel_width(self) -> int:
        return self.cell_size * self.width

    def get_pixel_heigth(self) -> int:
        return self.cell_size * self.heigth

    def render(self, border=True) -> None:
        border_color = pygame.Color("white")
        for x in range(self.width):
            for y in range(self.heigth):
                fill_color = pygame.Color(COLOR_MAP[self.board[x][y]])
                rect = pygame.Rect((x * self.cell_size + self.left, y *
                                    self.cell_size + self.top), (self.cell_size, ) * 2)
                pygame.draw.rect(screen, fill_color, rect, 0)
                if border:
                    pygame.draw.rect(screen, border_color, rect, 1)

    def relative_pos(self, pos: tuple) -> tuple:
        return (pos[0] - self.left, pos[1] - self.top)

    def get_click(self, pos):
        cell = self.get_cell(pos)
        self.on_click(cell)

    def on_click(self, cell):
        print(cell)

    def get_cell(self, pos: tuple) -> tuple:
        if all(map(lambda pos, maximum: 0 < pos < maximum, self.relative_pos(pos), self.get_pixel_size())):
            return tuple(coord // self.cell_size for coord in self.relative_pos(pos))
        else:
            return None


class Life(Board):
    def on_click(self, cell):
        x, y = cell
        self.board[x][y] = {1: 0, 0: 1}[self.board[x][y]]

    def next_step(self):
        neighbours_map = [[0] * self.heigth for _ in range(self.width)]
        for x in range(len(self.board)):
            for y in range(len(self.board[x])):
                neighbours = 0
                for shift_x in (-1, 0, 1):
                    for shift_y in (-1, 0, 1):
                        if shift_x == shift_y == 0:
                            continue
                        neighbours += self.board[x + shift_x][y + shift_y]
                neighbours_map[x][y] = neighbours
        for x in range(len(self.board)):
            for y in range(len(self.board[x])):
                cell = self.board[x][y]
                neighbours = neighbours_map[x][y]
                if cell == 1 and (neighbours < 2 or neighbours > 3):
                    self.board[x][y] = 0
                elif cell == 0 and neighbours == 3:
                    self.board[x][y] = 1


running = True
run_fps = 10
freeze_fps = 30
screen_heigth, screen_width = 520, 520

board = Life(25, 25)
life_running = False

pygame.init()
screen = pygame.display.set_mode((screen_width, screen_heigth))
clock = pygame.time.Clock()
while running:
    for event in pygame.event.get():
        if event.type == pygame.QUIT:
            running = False
        elif event.type == pygame.MOUSEBUTTONDOWN:
            if event.button == 1:
                board.get_click(event.pos)
            if event.button == 3:
                life_running = not life_running
            if event.button == 4:
                run_fps += 5
            if event.button == 5:
                run_fps -= 5
                if run_fps <= 0:
                    run_fps = 1
    screen.fill(pygame.Color("black"))
    if life_running:
        board.next_step()
    board.render()
    pygame.display.flip()
    if life_running:
        clock.tick(run_fps)
    else:
        clock.tick(freeze_fps)
pygame.quit()
