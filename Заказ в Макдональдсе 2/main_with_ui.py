from sys import argv, exit
from PyQt5.QtWidgets import QApplication, QMainWindow, QCheckBox, QSpinBox
from PyQt5 import QtCore, QtGui, QtWidgets


class Ui_MainWindow(object):
    def setupUi(self, MainWindow):
        MainWindow.setObjectName("MainWindow")
        MainWindow.resize(379, 301)
        self.centralwidget = QtWidgets.QWidget(MainWindow)
        self.centralwidget.setObjectName("centralwidget")
        self.gridLayout = QtWidgets.QGridLayout(self.centralwidget)
        self.gridLayout.setObjectName("gridLayout")
        self.verticalLayout = QtWidgets.QVBoxLayout()
        self.verticalLayout.setObjectName("verticalLayout")
        self.gridLayout.addLayout(self.verticalLayout, 0, 0, 1, 1)
        self.verticalLayout_2 = QtWidgets.QVBoxLayout()
        self.verticalLayout_2.setObjectName("verticalLayout_2")
        self.gridLayout.addLayout(self.verticalLayout_2, 0, 1, 1, 1)
        self.plainTextEdit = QtWidgets.QPlainTextEdit(self.centralwidget)
        sizePolicy = QtWidgets.QSizePolicy(
            QtWidgets.QSizePolicy.Expanding, QtWidgets.QSizePolicy.Expanding)
        sizePolicy.setHorizontalStretch(0)
        sizePolicy.setVerticalStretch(0)
        sizePolicy.setHeightForWidth(
            self.plainTextEdit.sizePolicy().hasHeightForWidth())
        self.plainTextEdit.setSizePolicy(sizePolicy)
        self.plainTextEdit.setVerticalScrollBarPolicy(
            QtCore.Qt.ScrollBarAsNeeded)
        self.plainTextEdit.setHorizontalScrollBarPolicy(
            QtCore.Qt.ScrollBarAsNeeded)
        self.plainTextEdit.setSizeAdjustPolicy(
            QtWidgets.QAbstractScrollArea.AdjustToContents)
        self.plainTextEdit.setDocumentTitle("")
        self.plainTextEdit.setLineWrapMode(QtWidgets.QPlainTextEdit.NoWrap)
        self.plainTextEdit.setReadOnly(True)
        self.plainTextEdit.setPlainText("")
        self.plainTextEdit.setObjectName("plainTextEdit")
        self.gridLayout.addWidget(self.plainTextEdit, 0, 2, 1, 1)
        spacerItem = QtWidgets.QSpacerItem(
            272, 20, QtWidgets.QSizePolicy.Expanding, QtWidgets.QSizePolicy.Minimum)
        self.gridLayout.addItem(spacerItem, 1, 2, 1, 1)
        self.pushButton = QtWidgets.QPushButton(self.centralwidget)
        sizePolicy = QtWidgets.QSizePolicy(
            QtWidgets.QSizePolicy.Minimum, QtWidgets.QSizePolicy.Fixed)
        sizePolicy.setHorizontalStretch(0)
        sizePolicy.setVerticalStretch(0)
        sizePolicy.setHeightForWidth(
            self.pushButton.sizePolicy().hasHeightForWidth())
        self.pushButton.setSizePolicy(sizePolicy)
        self.pushButton.setObjectName("pushButton")
        self.gridLayout.addWidget(self.pushButton, 1, 0, 1, 2)
        MainWindow.setCentralWidget(self.centralwidget)

        self.retranslateUi(MainWindow)
        QtCore.QMetaObject.connectSlotsByName(MainWindow)

    def retranslateUi(self, MainWindow):
        _translate = QtCore.QCoreApplication.translate
        MainWindow.setWindowTitle(_translate("MainWindow", "MainWindow"))
        self.plainTextEdit.setPlaceholderText(_translate(
            "MainWindow", "Тут будет отображён Ваш чек"))
        self.pushButton.setText(_translate("MainWindow", "Заказать"))


class MainWindow(QMainWindow, Ui_MainWindow):
    def __init__(self, products):
        super().__init__()
        self.setupUi(self)
        self.retranslateUi(self)
        self._products = []
        for product in products:
            button = QCheckBox(self)
            button.setText(f"{product[0]} ({product[1]} руб)")
            spin = QSpinBox(self)
            spin.setMinimum(1)
            self._products.append((button, spin) + product)
            self.verticalLayout.addWidget(button)
            self.verticalLayout_2.addWidget(spin)
        self.pushButton.clicked.connect(self.gen_order)
        self.plainTextEdit.setPlainText("")

    def gen_order(self):
        self.plainTextEdit.setPlainText("")
        order = tuple(filter(lambda el: el[0].isChecked(), self._products))
        order_text = "\n".join(map(
            lambda prod: f"{prod[2]}\n{prod[3]}\t{prod[1].value()}\t{prod[3] * prod[1].value()}\n",
            order))
        summ = sum(map(lambda prod: prod[3] * prod[1].value(), order))
        self.plainTextEdit.appendPlainText(
            "Mc Donalds на ул. Пушкина, ИНН 133722866642069\nВаш заказ:\n")
        self.plainTextEdit.appendPlainText("цена\tкол-во\tитого\n")
        self.plainTextEdit.appendPlainText(order_text + "\n")
        self.plainTextEdit.appendPlainText(f"Итого: {summ} рублей")


def main():
    app = QApplication(argv)
    products = (("Бигмак", 250),
                ("Чизбургер", 200),
                ("Чикенбургер", 200),
                ("Гамбургер", 150),
                ("Картошка фри", 75),
                ("Наггетсы", 130),
                ("Кетчуп", 15),
                ("Кола", 50),
                ("Пепси", 60),
                ("Спрайт", 45))
    window = MainWindow(products)
    window.show()
    exit(app.exec_())


if __name__ == "__main__":
    main()
