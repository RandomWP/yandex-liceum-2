class Point:
    def __init__(self, name, x, y):
        self._name = name
        self._coords = (x, y)

    def get_x(self):
        return self._coords[0]

    def get_y(self):
        return self._coords[1]

    def get_coords(self):
        return self._coords

    def __str__(self):
        return f"{self._name}{self._coords}"

    def __invert__(self):
        return Point(self._name, *reversed(self._coords))

    def __repr__(self):
        return f"Point{(self._name, *self._coords)}"

    def __eq__(self, other):
        return self._name == other._name and self._coords == other._coords

    def __lt__(self, other):
        if self._name != other._name:
            return self._name < other._name
        elif self._coords[0] != other._coords[0]:
            return self._coords[0] < other._coords[0]
        elif self._coords[1] != other._coords[1]:
            return self._coords[1] < other._coords[1]
        else:
            return False

    def __gt__(self, other):
        if self._name != other._name:
            return self._name > other._name
        elif self._coords[0] != other._coords[0]:
            return self._coords[0] > other._coords[0]
        elif self._coords[1] != other._coords[1]:
            return self._coords[1] > other._coords[1]
        else:
            return False

    def __le__(self, other):
        if self._name != other._name:
            return self._name < other._name
        elif self._coords[0] != other._coords[0]:
            return self._coords[0] < other._coords[0]
        elif self._coords[1] != other._coords[1]:
            return self._coords[1] < other._coords[1]
        else:
            return True

    def __ge__(self, other):
        if self._name != other._name:
            return self._name > other._name
        elif self._coords[0] != other._coords[0]:
            return self._coords[0] > other._coords[0]
        elif self._coords[1] != other._coords[1]:
            return self._coords[1] > other._coords[1]
        else:
            return True

    def __ne__(self, other):
        return self._name != other._name or self._coords != other._coords

    def __hash__(self):
        return hash((self._name, self._coords))
