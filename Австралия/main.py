import os
import sys
import requests
import pygame

map_req = "https://static-maps.yandex.ru/1.x/?ll=133.7751312,-25.2743988&spn=20,20&l=sat"
map_file = "map.png"
screen_size = (600, 450)
response = requests.get(map_req)

if not response:
    print("Error at request: ", map_req)
    print("HTTP status:", response.status_code, "(", response.reason, ")")
    sys.exit(1)

with open(map_file, "wb") as f:
    f.write(response.content)

pygame.init()
screen = pygame.display.set_mode(screen_size)
screen.blit(pygame.image.load(map_file), (0, 0))
pygame.display.flip()
os.remove(map_file)
while pygame.event.wait().type != pygame.QUIT:
    pass
pygame.quit()
