import sqlite3

db_name = input()
db = sqlite3.connect(db_name)
cur = db.cursor()
res = cur.execute("""SELECT title FROM Films
    WHERE duration >= 60  AND genre IN (
    SELECT id FROM genres
        WHERE title == "комедия")
""")
print(*map(lambda val: val[0], res), sep="\n")
cur.close()
