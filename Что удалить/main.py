import argparse
import os


def human_read_format(size):
    names = ["", "К", "М", "Г", "T"]
    step = 0
    while size >= 1024:
        step += 1
        size /= 1024
    return str(round(size)) + names[step] + "Б"


parser = argparse.ArgumentParser()
parser.add_argument("path", type=str)
args = parser.parse_args()

full_path = os.path.abspath(args.path)

dirs = []
for filename in os.listdir(full_path):
    path = full_path + "/" + filename
    if os.path.isdir(path):
        el_count = len(os.listdir(path))
        content_size = sum(map(lambda path:
                               sum(map(lambda f: os.path.getsize(
                                   f"{path[0]}/{f}"), path[-1])),
                               filter(lambda pth: pth[-1], os.walk(path))))
        dirs.append((content_size, filename))
print(*map(lambda d: f"{d[1]}\t{human_read_format(d[0])}",
           sorted(dirs, reverse=True)[:10]), sep="\n")
