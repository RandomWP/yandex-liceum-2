from sys import argv, exit
from PyQt5 import uic
from PyQt5.QtWidgets import QApplication, QMainWindow


class MainWindow(QMainWindow):
    def __init__(self):
        super().__init__()
        self.initUI()

    def initUI(self):
        uic.loadUi("MainWindow.ui", self)
        self.pushButton.clicked.connect(self.load_data)
        self.pushButton.clicked.connect(self.analyze_data)

    def load_data(self):
        with open("input.txt", "r") as inp:
            self.data = inp.read()

    def analyze_data(self):
        char_dict = {}
        for char in self.data:
            if not char.isalnum():
                continue
            if char in char_dict:
                char_dict[char] += 1
            else:
                char_dict[char] = 1
        chars = char_dict.items()
        print(chars)
        def key(val): return val[1]
        max_char = max(chars, key=key)
        self.lineEdit.setText(max_char[0])
        self.lineEdit_2.setText(str(max_char[1]))
        min_char = min(chars, key=key)
        self.lineEdit_3.setText(min_char[0])
        self.lineEdit_4.setText(str(min_char[1]))


def main():
    app = QApplication(argv)
    window = MainWindow()
    window.show()
    exit(app.exec_())


if __name__ == "__main__":
    main()
