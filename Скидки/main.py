import csv
with open("wares.csv") as csvfile:
    prods = csv.DictReader(csvfile, delimiter=';', quotechar='"')
    for prod in prods:
        if int(prod["Old price"]) > int(prod["New price"]):
            print(prod["Name"])
