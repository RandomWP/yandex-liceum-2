import pytest
from yandex_testing_lesson import reverse


def test_reverse_empty():
    assert reverse("") == ""


def test_reverse_one_char():
    assert reverse("a") == "a"
    assert reverse("1") == "1"
    assert reverse(" ") == " "


def test_reverse_palyndrome():
    assert reverse("aba") == "aba"
    assert reverse("1T34f6f43T1") == "1T34f6f43T1"
    assert reverse(".!.::;::.!.") == ".!.::;::.!."


def test_reverse_normal():
    assert reverse("abc") == "cba"
    assert reverse("erIUGEWf") == "fWEGUIre"
    assert reverse("54367") == "76345"


def test_reverse_noiterable():
    with pytest.raises(TypeError):
        reverse(2345)
    with pytest.raises(TypeError):
        reverse(345.456)
    with pytest.raises(TypeError):
        reverse(None)
    with pytest.raises(TypeError):
        reverse(print)


def test_reverse_nostring():
    with pytest.raises(TypeError):
        reverse([])
    with pytest.raises(TypeError):
        reverse((1, 4, 5))
    with pytest.raises(TypeError):
        reverse({})
    with pytest.raises(TypeError):
        reverse(["345", "abcfd", 56, None])
    with pytest.raises(TypeError):
        reverse(map(lambda a: a ** 2), range(100))
