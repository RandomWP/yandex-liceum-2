import pygame
from math import sqrt


def main():
    size, hue = map(int, input().split())
    width, heigth = 300, 300
    color_front = pygame.Color("black")
    color_front.hsva = (hue, 100, 75, 100)
    color_up = pygame.Color("black")
    color_up.hsva = (hue, 100, 100, 100)
    color_right = pygame.Color("black")
    color_right.hsva = (hue, 100, 50, 100)
    pygame.init()
    screen = pygame.display.set_mode((width, heigth))
    screen.fill(pygame.Color("black"))
    left_up = (width // 2 - size * 0.75, heigth // 2 - size * 0.25)
    right_up = (width // 2 + size * 0.25, heigth // 2 - size * 0.25)
    right_down = (width // 2 + size * 0.25, heigth // 2 + size * 0.75)
    pygame.draw.rect(screen, color_front, (*left_up, size, size), 0)
    pygame.draw.polygon(screen, color_up, (left_up,
                                           (left_up[0] + size // 2,
                                            left_up[1] - size // 2),
                                           (right_up[0] + size // 2,
                                            right_up[1] - size // 2),
                                           right_up))
    pygame.draw.polygon(screen, color_right, (right_up,
                                              (right_up[0] + size // 2,
                                               right_up[1] - size // 2),
                                              (right_down[0] + size // 2,
                                               right_down[1] - size // 2),
                                              right_down))
    pygame.display.flip()
    while pygame.event.wait().type != pygame.QUIT:
        pass


if __name__ == "__main__":
    main()
