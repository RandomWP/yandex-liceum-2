from sys import argv, exit
from PyQt5 import uic
from PyQt5.QtWidgets import QApplication, QMainWindow, QMessageBox, QCheckBox


class MainWindow(QMainWindow):
    def __init__(self, products):
        super().__init__()
        uic.loadUi("MainWindow.ui", self)
        self._products = []
        for product in products:
            button = QCheckBox(self)
            button.setText(f"{product[0]} ({product[1]} руб)")
            self._products.append((button,) + product)
            self.verticalLayout.addWidget(button)
        self.pushButton.clicked.connect(self.gen_order)
        self.plainTextEdit.setPlainText("")

    def gen_order(self):
        self.plainTextEdit.setPlainText("")
        order = tuple(filter(lambda el: el[0].isChecked(), self._products))
        order_text = "\n".join(
            map(lambda prod: f"{prod[1]} \t Цена: {prod[2]} руб.", order))
        summ = sum(map(lambda prod: prod[2], order))
        self.plainTextEdit.appendPlainText(
            "Mc Donalds на ул. Пушкина, ИНН 133722866642069\nВаш заказ:\n")
        self.plainTextEdit.appendPlainText(order_text + "\n")
        self.plainTextEdit.appendPlainText(f"Итого: {summ} рублей")


def main():
    app = QApplication(argv)
    products = (("Бигмак", 250),
                ("Чизбургер", 200),
                ("Чикенбургер", 200),
                ("Гамбургер", 150),
                ("Картошка фри", 75),
                ("Наггетсы", 130),
                ("Кетчуп", 15),
                ("Кола", 50),
                ("Пепси", 60),
                ("Спрайт", 45))
    window = MainWindow(products)
    window.show()
    exit(app.exec_())


if __name__ == "__main__":
    main()
