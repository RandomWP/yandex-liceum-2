import asyncio
import urllib
import pymorphy2
from discord.ext import commands

MORPH = pymorphy2.MorphAnalyzer()

TOKEN = ""


class Morph(commands.Cog):
    def __init__(self, bot):
        self.bot = bot

    @commands.command(name="help_bot")
    async def help_bot(self, ctx):
        msg = """Commands:
#!numerals [word] [numeral] - agreement with numerals
#!alive [word] - for define alive or not alive
#!noun [word] [case] [state] - for noun case (nomn, gent, datv, accs, ablt, loct) and numeral state (sing, plur)
#!inf [word] - for infinitive state
#!morph [word] - full morphological analysis"""
        await ctx.send(msg)

    @commands.command(name="inf")
    async def inf(self, ctx, word):
        pdata = MORPH.parse(word)[0]
        msg = pdata.normal_form
        await ctx.send(msg)

    @commands.command(name="alive")
    async def alive(self, ctx, word):
        pdata = MORPH.parse(word)[0]
        msg = f"{word} {'' if pdata.tag.animacy else 'не '}{MORPH.parse('живой')[0].inflect({pdata.tag.gender}).word}"
        await ctx.send(msg)

    @commands.command(name="numerals")
    async def numerals(self, ctx, word, num):
        num = int(num)
        pdata = MORPH.parse(word)[0]
        msg = f"{num} {pdata.make_agree_with_number(num).word}"
        await ctx.send(msg)

    @commands.command(name="noun")
    async def noun(self, ctx, word, case, number):
        pdata = MORPH.parse(word)[0]
        msg = pdata.inflect({case, number}).word
        await ctx.send(msg)

    @commands.command(name="morph")
    async def morph(self, ctx, word):
        pdata = MORPH.parse(word)[0]
        msg = pdata.tag.cyr_repr
        await ctx.send(msg)


bot = commands.Bot(command_prefix="#!")
bot.add_cog(Morph(bot))
bot.run(TOKEN)
