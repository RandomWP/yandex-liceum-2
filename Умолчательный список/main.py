class DefaultList(list):
    def __init__(self, default, *args, **kwargs):
        self._default = default
        super().__init__(*args, *kwargs)

    def __getitem__(self, index):
        try:
            return super().__getitem__(index)
        except IndexError:
            return self._default
