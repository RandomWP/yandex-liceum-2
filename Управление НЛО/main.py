from sys import argv, exit
from PyQt5.QtWidgets import QApplication, QLabel, QMainWindow
from PyQt5.QtCore import Qt
from PyQt5.QtGui import QPixmap


class MainWindow(QMainWindow):
    def __init__(self):
        super().__init__()
        self.initUi()

    def initUi(self):
        self.setGeometry(300, 300, 500, 500)
        pixmap = QPixmap("images/ufo.png").scaled(
            100, 100, Qt.KeepAspectRatio, Qt.SmoothTransformation)
        self.image = QLabel(self)
        self.image.setPixmap(pixmap)
        self.image.resize(100, 100)
        self.image.move(200, 200)

    def keyPressEvent(self, event):
        image_x, image_y = self.image.x(), self.image.y()
        window_x, window_y = self.width(), self.height()
        if event.key() == Qt.Key_Up:
            self.image.move(image_x, (image_y - 10) % (window_y - 50))
        elif event.key() == Qt.Key_Down:
            self.image.move(image_x, (image_y + 10) % (window_y - 50))
        elif event.key() == Qt.Key_Right:
            self.image.move((image_x + 10) % (window_x - 50), image_y)
        elif event.key() == Qt.Key_Left:
            self.image.move((image_x - 10) % (window_x - 50), image_y)


def main():
    app = QApplication(argv)
    window = MainWindow()
    window.show()
    exit(app.exec_())


if __name__ == "__main__":
    main()
