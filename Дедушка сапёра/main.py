import pygame
from random import randrange

COLOR_MAP = {-1: "black", 10: "red"}


class Board(object):
    def __init__(self, width: int, heigth: int, fillval: int = 0) -> None:
        self.width = width
        self.heigth = heigth
        self.board = [[fillval] * heigth for _ in range(width)]
        self.set_view(10, 10, 20)

    def set_view(self, left: int, top: int, cell_size: int) -> None:
        self.left = left
        self.top = top
        self.cell_size = cell_size

    def get_pixel_size(self) -> tuple:
        return (self.cell_size * self.width, self.cell_size * self.heigth)

    def get_pixel_width(self) -> int:
        return self.cell_size * self.width

    def get_pixel_heigth(self) -> int:
        return self.cell_size * self.heigth

    def render(self, border=True) -> None:
        border_color = pygame.Color("white")
        for x in range(self.width):
            for y in range(self.heigth):
                fill_color = pygame.Color(COLOR_MAP[self.board[x][y]])
                rect = pygame.Rect((x * self.cell_size + self.left, y *
                                    self.cell_size + self.top), (self.cell_size, ) * 2)
                pygame.draw.rect(screen, fill_color, rect, 0)
                if border:
                    pygame.draw.rect(screen, border_color, rect, 1)

    def relative_pos(self, pos: tuple) -> tuple:
        return (pos[0] - self.left, pos[1] - self.top)

    def get_click(self, pos):
        cell = self.get_cell(pos)
        self.on_click(cell)

    def on_click(self, cell):
        print(cell)

    def get_cell(self, pos: tuple) -> tuple:
        if all(map(lambda pos, maximum: 0 < pos < maximum, self.relative_pos(pos), self.get_pixel_size())):
            return tuple(coord // self.cell_size for coord in self.relative_pos(pos))
        else:
            return None


class Minesweeper(Board):

    def __init__(self, width, heigth, mines, fillval=0):
        super().__init__(width, heigth, fillval=fillval)
        for _ in range(mines):
            while mines > 0:
                x, y = randrange(width), randrange(heigth)
                if self.board[x][y] == 10:
                    continue
                self.board[x][y] = 10
                mines -= 1

    def on_click(self, cell):
        x, y = cell
        if self.board[x][y] != -1:
            return
        self.board[x][y] = 0
        for shift_x in (-1, 0, 1):
            for shift_y in (-1, 0, 1):
                if shift_x == shift_y == 0 or \
                        x + shift_x < 0 or \
                        x + shift_x >= self.width or \
                        y + shift_y < 0 or \
                        y + shift_y >= self.heigth:
                    continue
                if self.board[x + shift_x][y + shift_y] == 10:
                    self.board[x][y] += 1

    def render(self, border=True) -> None:
        border_color = pygame.Color("white")
        for x in range(self.width):
            for y in range(self.heigth):
                cell = self.board[x][y]
                x_pix, y_pix = x * self.cell_size + self.left, y * self.cell_size + self.top
                rect = pygame.Rect((x_pix, y_pix), (self.cell_size, ) * 2)
                if cell in COLOR_MAP:
                    fill_color = pygame.Color(COLOR_MAP[self.board[x][y]])
                if cell == -1 or cell == 10:
                    pygame.draw.rect(screen, fill_color, rect, 0)
                else:
                    font = pygame.font.Font(None, round(self.cell_size * 0.9))
                    text = font.render(str(cell), 1, pygame.Color("lightgreen"))
                    screen.blit(text, (x_pix + 2, y_pix + 2))
                if border:
                    pygame.draw.rect(screen, border_color, rect, 1)


running = True
run_fps = 100
screen_heigth, screen_width = 520, 520

board = Minesweeper(25, 25, 10, -1)
life_running = False

pygame.init()
screen = pygame.display.set_mode((screen_width, screen_heigth))
clock = pygame.time.Clock()
while running:
    for event in pygame.event.get():
        if event.type == pygame.QUIT:
            running = False
        elif event.type == pygame.MOUSEBUTTONDOWN:
            if event.button == 1:
                board.get_click(event.pos)
    screen.fill(pygame.Color("black"))
    board.render()
    pygame.display.flip()
    clock.tick(run_fps)
pygame.quit()
