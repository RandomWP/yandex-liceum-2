import sqlite3

db_name = input()
db = sqlite3.connect(db_name)
cur = db.cursor()
res = cur.execute("""SELECT DISTINCT year FROM Films WHERE title LIKE 'Х%'""")
print(*map(lambda val: val[0], res), sep="\n")
cur.close()
