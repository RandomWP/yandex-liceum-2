from sys import argv, exit
from PyQt5 import uic
from PyQt5.QtWidgets import QApplication, QMainWindow
from random import randrange


class MainWindow(QMainWindow):
    def __init__(self):
        super().__init__()
        self.initUI()

    def initUI(self):
        uic.loadUi("MainWindow.ui", self)
        self.pushButton.clicked.connect(self.load_file)

    def load_file(self):
        with open("input.txt", "r") as inp:
            lines = inp.readlines()
        self.lineEdit.setText(lines[randrange(0, len(lines))].strip())


def main():
    app = QApplication(argv)
    window = MainWindow()
    window.show()
    exit(app.exec_())


if __name__ == "__main__":
    main()
