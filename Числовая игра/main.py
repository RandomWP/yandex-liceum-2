from sys import argv, exit
from PyQt5 import uic
from PyQt5.QtWidgets import QApplication, QMainWindow, QMessageBox
from random import randint


class NumberGame(QMainWindow):
    def __init__(self):
        super().__init__()
        self.initUI()
        self.init_game()

    def init_game(self):
        self._num = randint(1, 10)
        self._plus = randint(1, 10)
        self._minus = randint(1, 10)
        self._moves = 10
        self.update_labels()
        self.update_buttons()

    def initUI(self):
        uic.loadUi("MainWindow.ui", self)
        self.buttonMinus.clicked.connect(self.num_substract)
        self.buttonPlus.clicked.connect(self.num_add)
        self.buttonReset.clicked.connect(self.init_game)

    def update_buttons(self):
        self.buttonMinus.setText(f"-{self._minus}")
        self.buttonPlus.setText(f"+{self._plus}")

    def make_move(self):
        self._moves -= 1
        self.update_labels()
        self.check_win_lose()

    def update_labels(self):
        self.labelMovesLeft.setText(f"Ост. ходов: {self._moves}")
        self.lcdNumber.display(self._num)

    def num_add(self):
        self._num += self._plus
        self.make_move()

    def num_substract(self):
        self._num -= self._minus
        self.make_move()

    def check_win_lose(self):
        if self._num == 0:
            msg = QMessageBox(self)
            msg.setText("Вы выиграли!")
        elif self._moves == 0:
            msg = QMessageBox(self)
            msg.setText("Вы проиграли!")
        else:
            return
        msg.show()
        self.init_game()


def main():
    app = QApplication(argv)
    game = NumberGame()
    game.show()
    exit(app.exec_())


if __name__ == "__main__":
    main()
