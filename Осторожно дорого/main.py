from sys import exit, argv
from csv import reader
from PyQt5 import uic
from PyQt5.QtWidgets import QApplication, QMainWindow, QFileDialog, QTableWidgetItem
from PyQt5.QtGui import QColor
from random import randint


class MainWindow(QMainWindow):
    def __init__(self):
        super().__init__()
        self.initUI()

    def initUI(self):
        uic.loadUi("MainWindow.ui", self)
        self.pushButton.clicked.connect(self.request_filename_and_fill_table)

    def fill_table(self, header, data):
        print(data)
        self.tableWidget.setColumnCount(len(header))
        self.tableWidget.setHorizontalHeaderLabels(header)
        self.tableWidget.setRowCount(0)
        for row_num, row_data in enumerate(data):
            self.tableWidget.setRowCount(self.tableWidget.rowCount() + 1)
            for el_num, el in enumerate(row_data):
                item = QTableWidgetItem(el)
                self.tableWidget.setItem(row_num, el_num, item)
        self.tableWidget.resizeColumnsToContents()

    def load_file(self, filename):
        with open(filename, "r") as csvfile:
            csv_reader = reader(csvfile, delimiter=';', quotechar='"')
            header = next(csv_reader)
            data = tuple(line for line in csv_reader)
        return header, data

    def request_filename(self):
        fname = QFileDialog.getOpenFileName(
            self, "Загрузить файл с ценами", "", "CSV files (*.csv)")[0]
        if fname:
            return fname
        raise FileNotFoundError

    def request_filename_and_fill_table(self):
        try:
            filename = self.request_filename()
        except FileNotFoundError:
            return
        header, data = self.load_file(filename)
        data = sorted(data, key=lambda val: int(val[1]), reverse=True)
        self.fill_table(header, data)
        self.color_table()

    def color_table(self):
        if self.tableWidget.rowCount() < 5:
            return
        for row in range(5):
            color = QColor(randint(0, 255), randint(0, 255), randint(0, 255))
            for col in range(self.tableWidget.columnCount()):
                item = self.tableWidget.item(row, col)
                item.setBackground(color)


def main():
    app = QApplication(argv)
    window = MainWindow()
    window.show()
    exit(app.exec_())


if __name__ == "__main__":
    main()
